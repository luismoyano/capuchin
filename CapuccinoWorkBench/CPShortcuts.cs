﻿using ComponentOwl.BetterListView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapuccinoWorkBench
{

    enum CPCommands {
        Begin, SaveExit, EditUnit, SaveEdit, Swap,
        CreateUnit, DeleteUnit, SplitUnit, JoinUnit,
        FocusToProps, CommitEdit, AddText, RemoveText, NONE

    }

    class CPShortcuts
    {
        private const string command = "Command";
        private const string blend = "Combination";
        private const string key = "Key";

        private const string shiftBlend = "Ctrl + Shift + ";
        private const string altBlend = "Ctrl + Alt + ";//For the future

        private string[] shiftKeys = new string[] {
            "A", "R", "U", "H", "Z", "C", "V", "B", "2", "4", "7", "9", "E"
        };

        private Dictionary<string, string> shiftList = new Dictionary<string, string>();
        private Dictionary<string, string> altList = new Dictionary<string, string>();

        private CPShortcutsView view;


        public CPShortcuts() {

            //Intento cargar los settings, si no hay, cargo default list
            ArrayList settings = Properties.Settings.Default.Shortcuts;
            populateLists(settings);
            
            //Inicializo el view
            initViews();

            
        }

        private void initViews(){
            view = new CPShortcutsView();

            //Empezamos a llenar el view
            view.list.BeginUpdate();

            view.list.Columns.AddRange(new string[] {command, blend, key});

            for (int i = 0; i < shiftList.Count; i++){
                view.list.Items.Add(new string[] {
                    shiftList.ElementAt(i).Value, //El comando
                    shiftBlend, //Ctrl + Shift
                    shiftList.ElementAt(i).Key // La key dada
                });
            }

            //Editar con un solo click
            view.list.LabelEditActivation = BetterListViewLabelEditActivation.SingleClick;
            
            //Queremos editar los subitems
            view.list.LabelEditModeSubItems = BetterListViewLabelEditMode.CustomControl;

            view.list.EndUpdate();

            // custom label editing needs to handle this event to obtain actual editing control
            view.list.RequestEmbeddedControl += ListViewRequestEmbeddedControl;

        }

        private IBetterListViewEmbeddedControl ListViewRequestEmbeddedControl(object sender, BetterListViewRequestEmbeddedControlEventArgs eventArgs){

            BetterListViewComboBoxEmbeddedControl comboBoxEmbeddedControl = new BetterListViewComboBoxEmbeddedControl();
            comboBoxEmbeddedControl.DropDownStyle = ComboBoxStyle.DropDownList;

            if (eventArgs.SubItem.Index == 1)
            {
                comboBoxEmbeddedControl.Items.AddRange(
                    new string[]{
                        shiftBlend//De momento sólo ctrl + shift
                    });

                return comboBoxEmbeddedControl;
            }
            else if (eventArgs.SubItem.Index == 2) {
                comboBoxEmbeddedControl.Items.AddRange(shiftList.Keys.ToArray());

                return comboBoxEmbeddedControl;
            }

            return null;
        }

        public void populateLists(ArrayList settings = null) {

            shiftList.Clear();
            altList.Clear();

            if (settings == null || settings.Count <= 0){
                for (int i = 0; i < shiftKeys.Length; i++){
                    string com = ((CPCommands)i).ToString();
                    Debug.Print(com);
                    shiftList.Add(shiftKeys[i], com);
                }
            }
            else {
                //Debug.Print("He pillado la lista desde los settings");
                for (int i = 0; i < settings.Count; i++){
                    string com = ((CPCommands)i).ToString();
                    Debug.Print(com);
                    shiftList.Add(settings[i].ToString(), com);
                }
            }
        }


        internal void display(){
            if (!view.Visible){
                view.ShowDialog();

                if (view.DialogResult == DialogResult.OK){
                    //Actualizo la lista
                    populateLists(new ArrayList(view.keys));

                    //Guardo las cosas en el fichero de settings
                    Properties.Settings.Default.Shortcuts = new ArrayList(view.keys);
                    //Properties.Settings.Default.Shortcuts = null;
                    Properties.Settings.Default.Save();
                }
            }
        }

        public CPCommands getCommand(Keys k) {

            Debug.Print(k.ToString());

            try{
                return (CPCommands)Enum.Parse(typeof(CPCommands), shiftList[k.ToString()]);
            }
            catch (KeyNotFoundException e) { return CPCommands.NONE; }
        }
    }
}
