﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Declaración de funcionalidades para la gestión de los documentos
/// </summary>
namespace CapuccinoWorkBench
{
    interface IDocumentManager{

        void createDocCopy(string path);

        bool doesCapuchinFileExist(string path);

        CapuchinFileResultCode loadCapuchinFile(string path, out List<Segment> segments);

        void saveCapuchinFile(List<Segment> segments, string path);
        
    }
}
