﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Clase contenedora de todo el texto input segmentado
/// Sobre esta clase se harán todas las operaciones concernientes a la segmentación de texto entrada
/// </summary>
namespace CapuccinoWorkBench
{
    class InputText
    {
        public StringBuilder rawInput { get; set; }
        public List<Segment> segmentedText;

        private SegmentTrade editable;

        public InputText() {
            rawInput = new StringBuilder();
            segmentedText = new List<Segment>();
        }

        public bool doesTextExist() {
            //return !String.IsNullOrEmpty(rawInput.ToString());//Testear
            return segmentedText.Count > 0;
        }

        internal void addText(string item, int ini, int end, bool firstFlag = false){
            Segment segment = new Segment(item, ini, end);
            safeAdd(segment, firstFlag);
        }

        private SegmentTrade getSegment(int cursor) { //fast af

            int begin = 0;
            int end = segmentedText.Count;
            int i = 0;
            int cummulator = 0;
            bool found = false;

            while (!found){
                i = (begin + end) / 2;
                Point range = segmentedText[i].getCurrentRange();

                if (cursor >= range.X && cursor <= range.Y){
                    found = true;
                    return new SegmentTrade(segmentedText[i], i);
                }
                else{
                    if (cursor < range.X){
                        end = i;
                        //Debug.Print("Me voy a la mitad ");
                    }
                    else{
                        if (cursor > range.Y){
                            begin = i;
                            //Debug.Print("Doblo ");

                        }
                    }
                }

                //En caso de que no encontremos nada
                cummulator++;
                found = cummulator >= segmentedText.Count;
            }
            return new SegmentTrade(true);//Nope
        }

        public string getSegmentString(int cursor) {
            Segment s = getSegment(cursor).segment;
            if (s != null) {
                return s.getOriginal();
            }
            return null;
        }

        public Point getSegmentRange(int cursor){
            Segment s = getSegment(cursor).segment;
            if (s != null) {
                return s.getOriginalRange();
            }
            return new Point();
        }

        internal Point getRange(int cursor)
        {
            int index = cursorToIndex(cursor);
            
            return segmentedText[index].getCurrentRange();

        }

        internal string getSegmentProposal(int cursor){
            int index = cursorToIndex(cursor);
            return segmentedText[index].getProcessed();
        }

        internal string[] getSegments(Point rng){

            Point indices = rangeToIndices(rng);//O(n)

            if (indices.X > -1 && indices.Y > -1) {

                List<string> seg = new List<string>();

                if (indices.X <= indices.Y){
                    foreach (Segment segment in segmentedText.GetRange(indices.X, (indices.Y - indices.X) + 1)){
                        seg.Add(segment.getOriginal());
                    }
                }
                return seg.ToArray();
            }
            return null;
        }

        

        internal void deleteByRange(Point rng){
            Point indices = rangeToIndices(rng);

            if (indices.X > -1 && indices.Y > -1){
                segmentedText.RemoveRange(indices.X, (indices.Y - indices.X) + 1);
            }
        }

        internal void deleteText(int cursor){
            int index = cursorToIndex(cursor);

            if(index != -1){segmentedText.RemoveAt(index);}
        }

        internal void joinUnits(Point rng) {
            Point indices = rangeToIndices(rng);
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < (indices.Y - indices.X) + 1; i++){
                builder.Append(segmentedText.ElementAt(indices.X + i).getOriginal());
                builder.Append(" ");//Preguntar
            }

            int start = segmentedText.ElementAt(indices.X).getCurrentRange().X;
            int end = segmentedText.ElementAt(indices.X + (indices.Y - indices.X)).getCurrentRange().Y;

            deleteByRange(rng);
            safeAdd(new Segment(builder.ToString(), start, end));

            //Debug.Print(rng.X + " " + rng.Y);
            
        }


        internal void split(int cursor){
            Point rng = getSegmentRange(cursor);
            Point index = rangeToIndices(rng);
            string left = getSegmentString(cursor);

            string right = left.Substring(cursor - rng.X);
            left = left.Remove(cursor - rng.X);

            segmentedText.RemoveAt(index.X);

            safeAdd(new Segment(left, rng.X, cursor));
            safeAdd(new Segment(right, cursor, rng.Y));
        }


        private Point rangeToIndices(Point rng){
            Point indices = new Point(
                getSegment(rng.X).index,
                getSegment(rng.Y).index
                );
            //Debug.Print("RangeToIndices = " + indices.ToString());
            return indices;
        }


        public Point[] getSpan(Point range)
        {
            List<Point> ranges = new List<Point>();
            Point indices = rangeToIndices(range);

            if (indices.X > -1 || indices.Y > -1) {

                if (indices.X < 0) indices.X = indices.Y;
                if (indices.Y < 0) indices.Y = indices.X;

                for (int i = 0; i < (indices.Y - indices.X) + 1; i++)
                {
                    ranges.Add(segmentedText[indices.X + i].getCurrentRange());
                }
            }
            
            //Debug.Print("GetSpan len = " + ranges.ToArray().Length);
            return ranges.ToArray();
        }

        

        private int cursorToIndex(int cursor){
            return getSegment(cursor).index;
        }

        private void safeAdd(Segment s, bool firstFlag = false) {
            //Adición y sort para que las operaciones puedan funcionar mejor.
            segmentedText.Add(s);

            if (!firstFlag){
                //Debug.Print("Hago un sort al añadir items!!");
                segmentedText.Sort(
                    delegate (Segment a, Segment b)
                    {
                        return a.getOriginalRange().X.CompareTo(b.getOriginalRange().X);
                    });
            }
        }

        internal Point getPreviousBlockSpan(int cursor){
            int prevIndex = cursorToIndex(cursor) -1;

            if (prevIndex >= 0){
                //Debug.Print("Intento coger el bloque previo de esta unidad");

                Debug.Print(prevIndex.ToString());
                Debug.Print(segmentedText[prevIndex].getOriginal());

                return new Point(0, segmentedText[prevIndex].getCurrentRange().Y);
            }
            return new Point(-1, -1);//No hay previous
        }

        internal Point getLatterBlockSpan(int cursor){
            int nextIndex = cursorToIndex(cursor) + 1;

            if (nextIndex < segmentedText.Count) {
                //Debug.Print("Intento coger el bloque siguiente de esta unidad");

                Debug.Print(nextIndex.ToString());
                Debug.Print(segmentedText[nextIndex].getOriginal());

                return new Point(
                    segmentedText[nextIndex].getCurrentRange().X,
                    segmentedText[segmentedText.Count - 1].getCurrentRange().Y
                );
            }
            return new Point(-1, -1);//No hay next
        }

        internal void stackEditUnit(int cursor){
            
            editable = getSegment(cursor);

            //Setteamos el estado
            segmentedText[cursorToIndex(cursor)].state = CPSegstate.EDITING;

            /*
            Debug.Print("Me guardo un editable");
            Debug.Print("Indice del unit = " + editable.index);
            */
        }

        internal void saveProposal(string newTxt){
            if (!editable.empty) {
                Point oRng = segmentedText[editable.index].getCurrentRange();

                //Reindexamos
                offsetAfter(editable.index, newTxt);

                segmentedText[editable.index].setProcessed(oRng.X, oRng.X + newTxt.Length, newTxt);

                segmentedText[editable.index].isOriginalDisplayed = false;

                segmentedText[editable.index].state = CPSegstate.SAVE;

                editable.empty = true;

            }
        }

        private void offsetAfter(int index, string txt = null) {

            int offset = segmentedText[index].getOffset(
                txt != null ? txt.Length : -1
                );
            
            if (offset != 0)
            {
                for (int i = index + 1; i < segmentedText.Count; i++){
                    segmentedText[i].offsetRangeByBuffer(offset+1);
                }
            }
        }

        internal string getOppositeContent(int cursor){//Esto dará problemas
            int index = cursorToIndex(cursor);

            if (segmentedText[index].isOriginalDisplayed){
                return segmentedText[index].getProcessed();
            }
            else {
                return segmentedText[index].getOriginal();
            }
        }

        internal void swap(int cursor) {

            int index = cursorToIndex(cursor);
            segmentedText[index].isOriginalDisplayed = !segmentedText[index].isOriginalDisplayed;

            offsetAfter(index);
        }


        internal CPSegstate getUnitState(int cursor){
            int index = cursorToIndex(cursor);

            
            return index == -1? CPSegstate.NOTHING : segmentedText[index].state;
        }

        internal void setStateCommit(int cursor){
            segmentedText[cursorToIndex(cursor)].state = CPSegstate.COMMIT;
        }
    }
}