﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapuccinoWorkBench
{
    public partial class CPStarterDialog : Form{

        public static string newFile = "CREATE AS NEW";
        public string filePath;

        public CPStarterDialog(){
            InitializeComponent();

            this.Shown += lookForCPFiles;
        }

        private void lookForCPFiles(object sender, EventArgs e){

            filePath = Globals.ThisAddIn.Application.ActiveDocument.FullName;
            filePath = Path.ChangeExtension(filePath, CPDocumentManager.cpFileExtension);

            if (File.Exists(filePath)){//Funciona
                filetxtbox.Text = filePath;
                filetxtbox.Enabled = false;

                //Busco la base de datos
            }
            else {
                fileNew_Click(null, null);//Simulo un click a 'New button'
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

            this.DialogResult = DialogResult.OK; 
            this.Close();
        }

        private void nopeBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void dbNewBtn_Click(object sender, EventArgs e){
            dbtxtbox.Text = newFile;
            dbtxtbox.Enabled = false;
        }

        private void fileNew_Click(object sender, EventArgs e){
            filetxtbox.Text = newFile;
            filetxtbox.Enabled = false;
        }

        private void dbBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK){
                
                dbtxtbox.Text = dialog.FileName;
            }
        }
    }
}
