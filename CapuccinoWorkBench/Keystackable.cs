﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapuccinoWorkBench
{
    class KeyStackable{

        private bool _forth;
        public bool forth { get { return _forth; } }

        private Keys _key;
        public Keys key { get { return _key; } }

        public KeyStackable() {
            _forth = true;
            _key = Keys.None;
        }

        public KeyStackable(Keys k, bool f = true) {
            _key = k;
            _forth = f;
        }

    }

}
