﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapuccinoWorkBench
{

    enum ClipBoardAction {CUT, COPY, PASTE}

    class ClipBoardEventArgs : EventArgs{
        ClipBoardAction action;
        String boardContent;
    }
}
