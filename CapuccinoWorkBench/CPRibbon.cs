﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using Microsoft.Office.Tools;
using System.Diagnostics;

namespace CapuccinoWorkBench
{
    public partial class CPRibbon
    {
        //Igual podría poner aquí una interfaz de shortcuts

        private void CPRibbon_Load(object sender, RibbonUIEventArgs e){
            //Cargo el ribbon

            setButtonsAsIdle();
        }

        public void disableAllButtons() {
            //Desactivamos todo por defecto
            beginBtn.Enabled = false;
            saveBtn.Enabled = false;

            EditModebtn.Enabled = false;
            saveEditBtn.Enabled = false;
            saveOriBtn.Enabled = false;

            focusPropsBtn.Enabled = false;

            saveUnitToDbBtn.Enabled = false;

            createUnitBtn.Enabled = false;
            ignoreUnitBtn.Enabled = false;
            joinUnitsBtn.Enabled = false;
            splitUnitsBtn.Enabled = false;
            //Globals.Ribbons.CPRibbon.swapBtn.Enabled = false;

            addTxtBtn.Enabled = false;
            deleteTxtBtn.Enabled = false;
        }

        public void setButtonsAsIdle() {

            disableAllButtons();

            beginBtn.Enabled = true;
        }

        private void helperDisplayBtn_Click(object sender, RibbonControlEventArgs e){
            CustomTaskPane pane = Globals.ThisAddIn.getSelfPanel();

            if (pane == null) Globals.ThisAddIn.AddAllTaskPanes();
            else pane.Visible = true;
        }

        private void createDbBtn_Click(object sender, RibbonControlEventArgs e){
            //Globals.ThisAddIn.createNewDataBaseFile(); //Disabled de momento
            
        }

        private void loadDbBtn_Click(object sender, RibbonControlEventArgs e){
            //Disabled de momento

            //Globals.ThisAddIn.loadDBFile();
        }

        private void saveUnitToDbBtn_Click(object sender, RibbonControlEventArgs e){
            Globals.ThisAddIn.commitEdit();
        }

        private void createUnitBtn_Click(object sender, RibbonControlEventArgs e){
            Globals.ThisAddIn.createUnit();
        }

        private void ignoreUnitBtn_Click(object sender, RibbonControlEventArgs e){
            Globals.ThisAddIn.deleteUnit();
        }

        private void joinUnitsBtn_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.joinUnits();
        }

        private void splitUnitsBtn_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.splitUnit();
        }

        private void aboutbtn_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.showAbout();
        }

        private void EditModebtn_Click(object sender, RibbonControlEventArgs e){
            Globals.ThisAddIn.editUnit();
        }

        private void saveEditBtn_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.saveEdit();
        }

        private void swapBtn_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.swapUnitNProposal();
        }

        private void focusPropsBtn_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.focusToPropsList();
        }

        private void beginBtn_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.startCapuchinSession();
        }

        private void saveBtn_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.saveSession();
        }

        private void shortcutsBtn_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.displayShortcutsView();
        }

        
    }
}
