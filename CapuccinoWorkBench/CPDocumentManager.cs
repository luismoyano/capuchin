﻿using System;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace CapuccinoWorkBench
{

    enum CapuchinFileResultCode {
        BADFILE,
        WRONGVERSION,
        ALREADYLOADED,
        SUCCESS
    }

    class CPDocumentManager : IDocumentManager{

        public static string cpFileExtension = ".capuchin";
        private const string copy = "-copy";

        private const string title = "Capuchin";
        private const string fver = "fileVer";
        private const string versionNumber = "0.1";

        private const string dbTitle = "Database";
        private const string pathName = "dbpath";

        private const string segCount = "segCount";

        private const string wordTitle = "WordFile";

        private const string segmentsStart = "Segments zone starts here";

        private const string segmentsZone = "Segments";

        private const string singleSegment = "Segment";
        private const string segIndex = "Indice";

        private const string langCommentStart = "Text here";
        private const string endComment = "--------";

        private const string ori = "Ori";
        private const string prop = "Prop";

        private const string oriRX = "OriRanX";
        private const string oriRY = "OriRanY";

        private const string prRX = "PropRanX";
        private const string prRY = "PropRanY";

        private const string state = "State";

        private const string display = "displayed";


        XmlReader reader;

        XmlWriter writer;
        XmlWriterSettings wSettings;

        private bool isLoaded = false;

        //XmlDeclaration declaration;// = doc.CreateXmlDeclaration("1.0", "UTF-8", "yes");

        string storePath;//Para guardar el path en caso de doc nuevo

        public CPDocumentManager() {
            wSettings = new XmlWriterSettings();
            wSettings.Indent = true;
        }


        public void createDocCopy(string path){
            if (File.Exists(path)){
                string nFile = Path.ChangeExtension(path, "");
                nFile = nFile.Remove(nFile.Length - 1);//Quito el punto del final

                string extension = Path.GetExtension(path);
                nFile += copy + extension;

                storePath = Path.ChangeExtension(path, cpFileExtension);

                if (!File.Exists(nFile))
                {
                    //No hay copia, podemos hacerla

                    Debug.Print("Copio el word en " + path + " " + nFile);
                    
                    File.Copy(path, nFile);
                    
                }
            }
            else {
                Debug.Print("El fichero no existe" + path);
            }
        }

        public bool doesCapuchinFileExist(string path){

            string ext = Path.GetExtension(path);

            if (ext == cpFileExtension) {
                return File.Exists(path);
            }
            return false;
        }

        public CapuchinFileResultCode loadCapuchinFile(string path, out List<Segment> segments)
        {
            //Leer del XML
            segments = new List<Segment>();

            List<Segment> s = new List<Segment>();


            if (isLoaded) return CapuchinFileResultCode.ALREADYLOADED;
            if (!(Path.GetExtension(path) == cpFileExtension) || !File.Exists(path)) return CapuchinFileResultCode.BADFILE;

            reader = XmlReader.Create(path);

            int ind = -1;

            string origin = "";
            string proposal = "";

            int orix = -1;
            int oriy = -1;

            int propx = -1;
            int propy = -1;

            CPSegstate st = CPSegstate.NOTHING;//For now
            bool disp = true;

            int sCapacity = -1;
            int addCount = 0;

            while (!reader.EOF)
            {
                if (reader.NodeType == XmlNodeType.Element)
                { // The node is an element.
                    switch (reader.Name)
                    {

                        case title: //Hemos dado con la cabecera
                            reader.MoveToAttribute(fver);
                            if (reader.Value != versionNumber) return CapuchinFileResultCode.WRONGVERSION;//Hacemos check de versión


                            //Agarro el conteo de items
                            reader.MoveToAttribute(segCount);

                            sCapacity = Convert.ToInt32(reader.Value);
                            s.Capacity = sCapacity;
                            break;

                        case singleSegment:
                            reader.MoveToFirstAttribute();
                            ind = reader.ReadContentAsInt();
                            break;

                        case ori:
                            origin = reader.ReadElementContentAsString();
                            break;

                        case oriRX:
                            orix = reader.ReadElementContentAsInt();
                            break;

                        case oriRY:
                            oriy = reader.ReadElementContentAsInt();
                            break;

                        case prop:
                            proposal = reader.ReadElementContentAsString();
                            break;

                        case prRX:
                            propx = reader.ReadElementContentAsInt();
                            break;

                        case prRY:
                            propy = reader.ReadElementContentAsInt();
                            break;

                        case state:
                            //Capturo el string
                            string enumStr = reader.ReadElementContentAsString();

                            //Lo convierto al enum
                            st = (CPSegstate)Enum.Parse(typeof(CPSegstate), enumStr);

                            break;

                        case display:
                            disp = Convert.ToBoolean(reader.ReadElementContentAsString());
                            break;
                    }
                }
                else if (reader.NodeType == XmlNodeType.EndElement)
                {
                    if (reader.Name == singleSegment)
                    {
                        //Añado el segment
                        Segment seg = new Segment(origin, orix, oriy);
                        seg.state = st;
                        seg.isOriginalDisplayed = disp;

                        if (propx != -1 && propy != -1)
                        {
                            seg.setProcessed(propx, propy, proposal);
                        }

                        s.Insert(ind, seg);
                        addCount++;
                        //s[ind] = seg;
                        //s.Add(seg);

                        //reseteo los valores para el próximo segmento
                        ind = -1;

                        origin = "";
                        proposal = "";

                        orix = -1;
                        oriy = -1;

                        propx = -1;
                        propy = -1;

                        st = CPSegstate.NOTHING;//For now
                        disp = true;
                        
                    }

                    //fuerzo el carro
                    reader.Read();
                }
                else {
                    //Fuerzo el avance
                    reader.Read();
                }

                Debug.Print("<" + reader.Name + ">");
            }

            //Chequeamos que todo se haya cargado correctamente
            if (addCount != sCapacity) return CapuchinFileResultCode.BADFILE;

            //Cierro el reader
            reader.Close();

            Debug.Print("Fichero cargado exitosamente");

            isLoaded = true;

            segments = s;

            return CapuchinFileResultCode.SUCCESS;
        }

        public void saveCapuchinFile(List<Segment> segments, string path = null){

            if (storePath != null)
            {
                isLoaded = false;


                Debug.Print("Guardo el XML en el path " + storePath);
                writer = XmlWriter.Create(storePath);


                writer.WriteStartDocument();

                writer.WriteStartElement(title);
                writer.WriteAttributeString(fver, versionNumber);
                writer.WriteAttributeString(pathName, "TODO");
                writer.WriteAttributeString(segCount, segments.Count.ToString());

                writer.WriteComment(segmentsStart);


                for (int i = 0; i < segments.Count; i++)
                {

                    writer.WriteStartElement(singleSegment);
                    writer.WriteAttributeString(segIndex, i.ToString());

                    //writer.WriteComment(langCommentStart);

                    writer.WriteElementString(ori, segments[i].getOriginal());
                    writer.WriteElementString(oriRX, segments[i].getOriginalRange().X.ToString());
                    writer.WriteElementString(oriRY, segments[i].getOriginalRange().Y.ToString());

                    if (segments[i].getProcessed() != "")
                    {
                        writer.WriteElementString(prop, segments[i].getProcessed());
                        writer.WriteElementString(prRX, segments[i].getProcessedRange().X.ToString());
                        writer.WriteElementString(prRY, segments[i].getProcessedRange().Y.ToString());
                    }

                    writer.WriteElementString(state, segments[i].state.ToString());

                    writer.WriteElementString(display, segments[i].isOriginalDisplayed.ToString());
                    writer.WriteEndElement();

                }
                writer.WriteFullEndElement();
                writer.WriteEndDocument();

                writer.Flush();
                writer.Close();
            }
        }
    }
}
