﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
namespace CapuccinoWorkBench{

    enum CPDataBaseResultCode
    {
        SUCCESS,
        FILEALREADYEXISTS,
        DBALREADYLOADED,
        NODBLOADED,
        TABLEDOESNTEXIST,
        ROWALREADYEXISTS,
        SQLERROR,
        NOTADATABASE

    }

    class CPDataBaseHandler{
        
        SQLiteConnection connection;
        SQLiteCommand command;
        SQLiteDataReader reader;

        public bool loaded { get; set; }

        String dbPath;
        private const string dbExtension = ".db";

        public string dbName { get; internal set; }

        public CPDataBaseHandler() {

            //try{
                //ThisAddIn.print("INTENTO CREAR SQLITECONNECTION");
                connection = new SQLiteConnection();
            //}catch(Exception e){
                //ThisAddIn.print("NO PUEDO CONECTAR CON SQLITE");
                //ThisAddIn.print(e.StackTrace);
            //}
            dbPath = "";//Empty
            loaded = false;
        }

        public CPDataBaseResultCode createNewEmptyDataBase(String name) {
            if (dbPath == "") {
                dbPath = name + dbExtension;
                dbName = Path.GetFileName(dbPath);
                //SQLiteConnection.CreateFile(dbPath);
                connection = new SQLiteConnection("Data Source=" + dbPath + "; Version=3;");
                connection.Open();

                //MessageBox.Show(dbPath);

                return CPDataBaseResultCode.SUCCESS;
            }
            return CPDataBaseResultCode.DBALREADYLOADED;
        }

        public CPDataBaseResultCode loadExistingDataBase(String path) {//Incrementar
            if (dbPath == "") {
                try {
                    dbPath = path;
                    dbName = Path.GetFileName(dbPath);
                    if (dbPath.Substring(dbPath.Length - dbExtension.Length) != dbExtension) {
                        dbPath = "";
                        return CPDataBaseResultCode.NOTADATABASE;
                    }

                    connection = new SQLiteConnection(@"Data Source=" + path + "; Version=3;");
                    //connection = new SQLiteConnection("Data Source=C:\\Users\\Luis Carlos\\Desktop\\nooficial.sqlite; Version=3");
                    connection.Open();
                    return CPDataBaseResultCode.SUCCESS;
                }
                catch(Exception e){
                    //do something
                    dbPath = "";
                    return CPDataBaseResultCode.SQLERROR;
                }
            }
            return CPDataBaseResultCode.DBALREADYLOADED;
        }

        public CPDataBaseResultCode createCapuchinTables() {
            if (dbPath != "") {
                try{
                    //Original
                    command = new SQLiteCommand("CREATE TABLE IF NOT EXISTS origin (originId INTEGER PRIMARY KEY, content TEXT, UNIQUE(originId, content));", connection);
                    command.ExecuteNonQuery();

                    //Prop
                    command = new SQLiteCommand("CREATE TABLE IF NOT EXISTS proposals (propId INTEGER PRIMARY KEY, content TEXT, origin INT, FOREIGN KEY(origin) REFERENCES origin(originId), UNIQUE(propId, content, origin));", connection);
                    command.ExecuteNonQuery();

                    return CPDataBaseResultCode.SUCCESS;
                }
                catch (Exception e){
                    //something
                    return CPDataBaseResultCode.SQLERROR;
                }
            }
            return CPDataBaseResultCode.NODBLOADED;
        }

        public String[] retrieveProposalsByOrigin(String origin){
            if (dbPath != ""){
                try{
                    origin = origin.Replace("'", "''");//Failsafe para´textos con apóstrofes
                
                    //Debug.Print(cmd);
                    command = new SQLiteCommand("SELECT originId FROM origin WHERE content = '" + origin + "'", connection);
                    reader = command.ExecuteReader();

                    List<int> origins = new List<int>();
                    while (reader.Read()){
                        //Coger las rows
                        origins.Add(Int32.Parse(reader["originId"].ToString()));
                    }

                    List<String> proposals = new List<String>();
                    
                    foreach (int id in origins){
                        command = new SQLiteCommand("SELECT content FROM proposals WHERE origin = " + id , connection);
                        reader = command.ExecuteReader();
                       
                        while (reader.Read()) {
                            proposals.Add(reader["content"].ToString());
                        }
                    }
                    return proposals.ToArray();
                }
                catch (Exception e){
                    Debug.Print("Exception! en retrieve proposals by origin");
                    Debug.Print(e.StackTrace);
                    return null;
                }
                
            }
            return null;
        }

        public CPDataBaseResultCode addOriginAndProposal(String origin, String proposal) {

            if (dbPath != ""){
                try{
                    //No existe el origen, añadimos origen
                    command = new SQLiteCommand("INSERT INTO origin (content) values('" + origin + "')", connection);
                    command.ExecuteNonQuery();

                    
                    //No existe la propuesta, la añado
                    command = new SQLiteCommand("INSERT INTO proposals (content, origin) values('" + proposal + "', " +
                                                                                                "(SELECT originId FROM origin WHERE content = '" + origin + "'))", connection);
                    command.ExecuteNonQuery();
                    return CPDataBaseResultCode.SUCCESS;
                }
                catch (Exception e){
                    return CPDataBaseResultCode.SQLERROR;
                }

            }
            return CPDataBaseResultCode.NODBLOADED;
        }

        public bool doesDBExist() {
            return dbPath != "" && loaded;
        }

        public void closeDB() {
            if (doesDBExist()) {
                connection.Close();
                dbPath = "";
                dbName = "";
                loaded = false;
            }
        }

        
    }
}
