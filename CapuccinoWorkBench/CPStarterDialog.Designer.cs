﻿namespace CapuccinoWorkBench
{
    partial class CPStarterDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CPStarterDialog));
            this.dbtxtbox = new System.Windows.Forms.TextBox();
            this.filetxtbox = new System.Windows.Forms.TextBox();
            this.dblabel = new System.Windows.Forms.Label();
            this.filelabel = new System.Windows.Forms.Label();
            this.dbBtn = new System.Windows.Forms.Button();
            this.fileBtn = new System.Windows.Forms.Button();
            this.okBtn = new System.Windows.Forms.Button();
            this.nopeBtn = new System.Windows.Forms.Button();
            this.dbNewBtn = new System.Windows.Forms.Button();
            this.fileNew = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dbtxtbox
            // 
            this.dbtxtbox.Location = new System.Drawing.Point(86, 15);
            this.dbtxtbox.Name = "dbtxtbox";
            this.dbtxtbox.Size = new System.Drawing.Size(415, 20);
            this.dbtxtbox.TabIndex = 0;
            // 
            // filetxtbox
            // 
            this.filetxtbox.Location = new System.Drawing.Point(86, 41);
            this.filetxtbox.Name = "filetxtbox";
            this.filetxtbox.Size = new System.Drawing.Size(415, 20);
            this.filetxtbox.TabIndex = 1;
            // 
            // dblabel
            // 
            this.dblabel.AutoSize = true;
            this.dblabel.Location = new System.Drawing.Point(12, 18);
            this.dblabel.Name = "dblabel";
            this.dblabel.Size = new System.Drawing.Size(54, 13);
            this.dblabel.TabIndex = 2;
            this.dblabel.Text = "DataBase";
            // 
            // filelabel
            // 
            this.filelabel.AutoSize = true;
            this.filelabel.Location = new System.Drawing.Point(12, 44);
            this.filelabel.Name = "filelabel";
            this.filelabel.Size = new System.Drawing.Size(68, 13);
            this.filelabel.TabIndex = 3;
            this.filelabel.Text = "Capuchin file";
            // 
            // dbBtn
            // 
            this.dbBtn.Location = new System.Drawing.Point(507, 12);
            this.dbBtn.Name = "dbBtn";
            this.dbBtn.Size = new System.Drawing.Size(75, 23);
            this.dbBtn.TabIndex = 4;
            this.dbBtn.Text = "Browse";
            this.dbBtn.UseVisualStyleBackColor = true;
            this.dbBtn.Click += new System.EventHandler(this.dbBtn_Click);
            // 
            // fileBtn
            // 
            this.fileBtn.Location = new System.Drawing.Point(507, 44);
            this.fileBtn.Name = "fileBtn";
            this.fileBtn.Size = new System.Drawing.Size(75, 23);
            this.fileBtn.TabIndex = 5;
            this.fileBtn.Text = "Browse";
            this.fileBtn.UseVisualStyleBackColor = true;
            this.fileBtn.Click += new System.EventHandler(this.button2_Click);
            // 
            // okBtn
            // 
            this.okBtn.Location = new System.Drawing.Point(508, 79);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(75, 23);
            this.okBtn.TabIndex = 6;
            this.okBtn.Text = "Accept";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.button3_Click);
            // 
            // nopeBtn
            // 
            this.nopeBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.nopeBtn.Location = new System.Drawing.Point(589, 79);
            this.nopeBtn.Name = "nopeBtn";
            this.nopeBtn.Size = new System.Drawing.Size(75, 23);
            this.nopeBtn.TabIndex = 7;
            this.nopeBtn.Text = "Cancel";
            this.nopeBtn.UseVisualStyleBackColor = true;
            this.nopeBtn.Click += new System.EventHandler(this.nopeBtn_Click);
            // 
            // dbNewBtn
            // 
            this.dbNewBtn.Location = new System.Drawing.Point(588, 12);
            this.dbNewBtn.Name = "dbNewBtn";
            this.dbNewBtn.Size = new System.Drawing.Size(75, 23);
            this.dbNewBtn.TabIndex = 8;
            this.dbNewBtn.Text = "New";
            this.dbNewBtn.UseVisualStyleBackColor = true;
            this.dbNewBtn.Click += new System.EventHandler(this.dbNewBtn_Click);
            // 
            // fileNew
            // 
            this.fileNew.Location = new System.Drawing.Point(589, 43);
            this.fileNew.Name = "fileNew";
            this.fileNew.Size = new System.Drawing.Size(75, 23);
            this.fileNew.TabIndex = 9;
            this.fileNew.Text = "New";
            this.fileNew.UseVisualStyleBackColor = true;
            this.fileNew.Click += new System.EventHandler(this.fileNew_Click);
            // 
            // CPStarterDialog
            // 
            this.AcceptButton = this.okBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.nopeBtn;
            this.ClientSize = new System.Drawing.Size(669, 114);
            this.Controls.Add(this.fileNew);
            this.Controls.Add(this.dbNewBtn);
            this.Controls.Add(this.nopeBtn);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.fileBtn);
            this.Controls.Add(this.dbBtn);
            this.Controls.Add(this.filelabel);
            this.Controls.Add(this.dblabel);
            this.Controls.Add(this.filetxtbox);
            this.Controls.Add(this.dbtxtbox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CPStarterDialog";
            this.Text = "Begin Capuchin Session";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label dblabel;
        private System.Windows.Forms.Label filelabel;
        private System.Windows.Forms.Button dbBtn;
        private System.Windows.Forms.Button fileBtn;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.Button nopeBtn;
        public System.Windows.Forms.TextBox dbtxtbox;
        public System.Windows.Forms.TextBox filetxtbox;
        private System.Windows.Forms.Button dbNewBtn;
        private System.Windows.Forms.Button fileNew;
    }
}