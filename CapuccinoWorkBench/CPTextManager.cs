﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

using System.Diagnostics;//Print para debuggear, Eliminar para el pack final
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;

/// <summary>
/// Implementación concerniente al manejo del texto de entrada
/// </summary>
namespace CapuccinoWorkBench
{
    class CPTextManager : ITextManager
    {
        //Contenedor de texto
        private InputText text;




        public InputText inputText {
            get { return text; }
        }

        //Reglas de segmentación
        private SortedSet<string> splitRules;
        private string space = " ";

        

        public CPTextManager() {
            text = new InputText();

            setSplitRules();
        }

        void ITextManager.getDocumentText()
        {
            throw new NotImplementedException();
        }

        private void setSplitRules(){
            splitRules = new SortedSet<string>();

            splitRules.Add(",");//Separar cuando haya comas.
            splitRules.Add(".");//Separar cuando haya puntos seguidos
            splitRules.Add("\r");//Retorno de carro

            //No lo sé, pero por si acaso...
            splitRules.Add("\n");//Salto de línea
            splitRules.Add("\v");//Tab vertical
        }

        public bool hasGatheredText() {
            return text.doesTextExist();
        }

        public bool gatherText(StringBuilder gathered) {
            if (!hasGatheredText()) {
                
                //REGLA: Sólo se recopila texto una vez
                inputText.rawInput = gathered;

                int cursor = 0;
                StringBuilder str = new StringBuilder();

                for (int i = 0; i < gathered.Length; i++){

                    str.Append(gathered[i]);//Es un char accesible

                    if (splitRules.Contains(gathered[i].ToString())){

                        //Hacemos un split
                        inputText.addText(str.ToString(),
                            Char.IsLetterOrDigit(Char.Parse(gathered[cursor].ToString()))? cursor : cursor+1,
                            i+1, true
                        );
                        
                        str.Clear();
                        cursor = i+1;
                    }
                    
                }
                return true;//Success
            }
            return false;//Fail
        }

        public string getUnitByCursor(int cursor){
            if (inputText.doesTextExist()) {
                return inputText.getSegmentString(cursor);
            }
            return "";//Nope
        }

        public string getProposalByUnit(int cursor){
            if (inputText.doesTextExist()) {
                return inputText.getSegmentProposal(cursor);
            }
            return "";
        }

        public Point getUnitRangeByCursor(int cursor)
        {
            if (inputText.doesTextExist())
            {
                return inputText.getRange(cursor);
                
            }
            return new Point();
        }

        public string[] getUnitsByRange(Point rng){
            if (inputText.doesTextExist()){
                return inputText.getSegments(rng);
            }
            return null;//Nope
        }

        public Point[] getUnitsSpanByRange(Point range){
            return inputText.getSpan(range);
        }


        public bool createUnitBySelection(Point rng, string txt){
            inputText.addText(txt, rng.X, rng.Y);
            return true;
        }

        public bool deleteUnitBySelection(Point rng){
            if (inputText.doesTextExist()){
                String[] target = inputText.getSegments(rng);

                if (target != null || target.Length > 0) {
                    inputText.deleteByRange(rng);
                    return true;
                }
            }
            return false;
        }

        public bool deleteUnitByCursor(int cursor){
            if (inputText.doesTextExist()) {
                inputText.deleteText(cursor);
                return true;
            }
            return false;
        }

        public bool joinUnitsByRange(Point rng){
            if (inputText.doesTextExist()) {
                inputText.joinUnits(rng);
                return true;
            }
            return false;
        }

        public bool splitUnit(int cursor){
            if (inputText.doesTextExist()) {
                inputText.split(cursor);
                return true;
            }
            return false;
        }


        public Point getFormerBlockSpan(int cursor){
            return inputText.getPreviousBlockSpan(cursor);
        }

        public Point getLatterBlockSpan(int cursor){
            return inputText.getLatterBlockSpan(cursor);
        }

        public void saveProposalInCurrentUnit(string newTxt){
            inputText.saveProposal(newTxt);
        }

        public void stackCurrentUnit(int cursor){
            if (inputText.doesTextExist()){
                inputText.stackEditUnit(cursor);
            }
        }


        public string getCurrentOpposite(int cursor){
            if (inputText.doesTextExist()) {
                return inputText.getOppositeContent(cursor);
            }
            return "";
        }

        public void swapUnitNProp(int cursor) {
            inputText.swap(cursor);
        }


        public CPSegstate getUnitState(int cursor){
            return inputText.getUnitState(cursor);
        }

        public void notifyCommit(int cursor){
            inputText.setStateCommit(cursor);
        }

        //////////////////Life savers/////////////////////////
        internal bool isSplitRule(string text){return splitRules.Contains(text);}
        
    }
}
