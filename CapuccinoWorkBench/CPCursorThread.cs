﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace CapuccinoWorkBench
{
    class CPCursorThread : CPThread{

        
        private float _latency = 0.5f;
        Timer timer;

        private Func<Point> runningFunction;

        public CPCursorThread() {
            timer = new Timer(_latency * 1000);
            runningFunction = null;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="latency">Latencia en segundos (0,5 por defecto) para invocar function, si latencia <= 0, se invoca function sólo una vez.</param>
        /// <param name="function">La función ha de recibir 0 parámetros y retornar un Point</param>
        public CPCursorThread(float latency, Func<Point> function) {
            _latency = latency;
            runningFunction = function;
            timer = new Timer(_latency * 1000);
        }


        public override void runThread(){
            if (_latency > 0){
                timer.AutoReset = true;//Loop
                timer.Elapsed += timerLooper;
                timer.Start();
                
            }
            else {//Sólo una vez
                runningFunction?.Invoke();
            }
        }

        private void timerLooper(object sender, ElapsedEventArgs e){
            runningFunction?.Invoke();
        }

        public void stopLooping() {
            timer.Stop();
        }

        public void pause() {
            timer.Enabled = false;
        }

        public void resume(){
            timer.Enabled = true;
            timer.Start();
        }
    }
}
