﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Runtime.InteropServices;
using Word = Microsoft.Office.Interop.Word;

namespace CapuccinoWorkBench
{

    [ComVisible(true)]
    interface IVBA {
        void callCapuchinFunction();
    }


    [ComVisible(true)]
    [ClassInterface(ClassInterfaceType.None)]
    class VBALink : IVBA{
        //Link entre VBA y Word para usar Capuchin con Shortcuts

        EventHandler functionHandler;

        public void callCapuchinFunction() {

            Word.Document activeDoc = Globals.ThisAddIn.Application.ActiveDocument;

            if (activeDoc != null) {
                //Llamar función
                ThisAddIn.print("Llamo desde VBA");
            }

        }
    }
}
