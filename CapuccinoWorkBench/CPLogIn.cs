﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapuccinoWorkBench
{


    enum CPLogInResultCode {
        SUCCESS, FAILURE
    }

    class CPLogIn{

        
        private CPStarterDialog logInDialog;

        public string cpfilePath;
        private string databasePath;

        public bool isGameOn { get; set; }

        public CPLogIn() {
            logInDialog = new CPStarterDialog();

            isGameOn = false;
        }

        public void startLogInProcess() {
            DialogResult res = logInDialog.ShowDialog();

            switch (res) {
                case DialogResult.OK:
                    //Debug.Print("El dialog de inicio de sesión devuelve ok!");

                    //Buscar si existe un documento similar

                    cpfilePath = logInDialog.filetxtbox.Text;
                    databasePath = logInDialog.dbtxtbox.Text;

                    Globals.ThisAddIn.beginSessionWithParams(databasePath, cpfilePath);
                     
                    break;
            }
        }

        public void notifyResult(CPLogInResultCode result) {
            if (result == CPLogInResultCode.SUCCESS){
                isGameOn = true;
                
            }
            else {
                //Enseñar un dialogo que avise que no se puede iniciar sesión correctamente
            }
        }

        internal string getDBPath(){
            return databasePath;
        }

        internal void logOut(){
            cpfilePath = "";
            databasePath = "";
            isGameOn = false;

            logInDialog.filetxtbox.Enabled = true;
            logInDialog.filetxtbox.Clear();

            logInDialog.dbtxtbox.Enabled = true;
            logInDialog.dbtxtbox.Clear();
        }
    }
}
