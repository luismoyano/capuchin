﻿namespace CapuccinoWorkBench
{
    partial class GeneralPrompt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGroup = new System.Windows.Forms.GroupBox();
            this.button2cancelBtn = new System.Windows.Forms.Button();
            this.acceptBtn = new System.Windows.Forms.Button();
            this.nameLabel = new System.Windows.Forms.Label();
            this.inputTxt = new System.Windows.Forms.TextBox();
            this.btnGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGroup
            // 
            this.btnGroup.Controls.Add(this.button2cancelBtn);
            this.btnGroup.Controls.Add(this.acceptBtn);
            this.btnGroup.Location = new System.Drawing.Point(290, 95);
            this.btnGroup.Name = "btnGroup";
            this.btnGroup.Size = new System.Drawing.Size(200, 39);
            this.btnGroup.TabIndex = 0;
            this.btnGroup.TabStop = false;
            // 
            // button2cancelBtn
            // 
            this.button2cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2cancelBtn.Location = new System.Drawing.Point(103, 10);
            this.button2cancelBtn.Name = "button2cancelBtn";
            this.button2cancelBtn.Size = new System.Drawing.Size(91, 23);
            this.button2cancelBtn.TabIndex = 1;
            this.button2cancelBtn.Text = "Cancel";
            this.button2cancelBtn.UseVisualStyleBackColor = true;
            // 
            // acceptBtn
            // 
            this.acceptBtn.Location = new System.Drawing.Point(6, 10);
            this.acceptBtn.Name = "acceptBtn";
            this.acceptBtn.Size = new System.Drawing.Size(91, 23);
            this.acceptBtn.TabIndex = 0;
            this.acceptBtn.Text = "Accept";
            this.acceptBtn.UseVisualStyleBackColor = true;
            this.acceptBtn.Click += new System.EventHandler(this.acceptBtn_Click);
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(16, 56);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(54, 13);
            this.nameLabel.TabIndex = 2;
            this.nameLabel.Text = "File Name";
            // 
            // inputTxt
            // 
            this.inputTxt.Location = new System.Drawing.Point(76, 53);
            this.inputTxt.Name = "inputTxt";
            this.inputTxt.Size = new System.Drawing.Size(414, 20);
            this.inputTxt.TabIndex = 3;
            // 
            // GeneralPrompt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 146);
            this.Controls.Add(this.inputTxt);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.btnGroup);
            this.Name = "GeneralPrompt";
            this.Text = "File name";
            this.btnGroup.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox btnGroup;
        private System.Windows.Forms.Button button2cancelBtn;
        private System.Windows.Forms.Button acceptBtn;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox inputTxt;
    }
}