﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapuccinoWorkBench
{
    struct CPProposal
    {
        int arrayPos { get; }
        string prop { get; }

        public CPProposal(int pos, string proposal) {
            arrayPos = pos;
            prop = proposal;
        }
    }
}
