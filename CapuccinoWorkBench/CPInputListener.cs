﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Forms;

namespace CapuccinoWorkBench
{

    /// <summary>
    /// Wrapper de Windows Hook para hacer cosas guays con eventos de teclado y ratón
    /// </summary>
    class CPInputListener{

        //Events
        public KeyEventHandler keyDown;
        public KeyEventHandler keyReleased;
        public KeyEventHandler keyPressed;
        public KeyEventHandler shortcutPressed;

        public EventHandler textPasted;
        public EventHandler textCut;
        public EventHandler textCopied;
        public EventHandler textSwapped;

        public string[] shortcuts;

        //hooking
        private delegate IntPtr hookProcedure(int ncode, IntPtr wParam, IntPtr lParam);


        //Hooks
        private const int WH_KEYBOARD = 2;
        private const int WH_MOUSE = 7;
        private const int HC_ACTION = 0;


        private hookProcedure proc;

        private static IntPtr hookID = IntPtr.Zero;

        //Enganches

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr SetWindowsHookEx(int hookId, hookProcedure proc, IntPtr hInstance, uint thread);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        private static extern bool unHookWindowsHookEx(int hookId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr CallNextHookEx(IntPtr hookId, int ncode, IntPtr wparam, IntPtr lparam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string name);

        [DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int GetCurrentThreadId();

        [DllImport("user32.dll")]
        private static extern short GetKeyState(int nVirtKey);

        public CPInputListener() {

            proc = keyBoardCallback;

            hookID = setHook(proc);
        }


        private IntPtr setHook(hookProcedure procedure){

            uint threadId = (uint)GetCurrentThreadId();

            return SetWindowsHookEx(WH_KEYBOARD, procedure, IntPtr.Zero, threadId);
        }

        public void stopListeningAll() {
            try
            {
                unHookWindowsHookEx(WH_KEYBOARD);//For now
            }
            catch (Exception e) { /**Nope**/}
        }
        

        private IntPtr keyBoardCallback(int ncode, IntPtr wParam, IntPtr lParam) {
            
            if (ncode == HC_ACTION) {

                Keys key = (Keys)wParam;
                KeyEventArgs args = new KeyEventArgs(key);

                bool isDown = ((ulong)lParam & 0x40000000) == 0;
                //Debug.Print("" + ((ulong)lParam & 0x40000000));
                if (isDown){
                    onKeyDown(args);//for now

                    //Shortcut
                    if (isKeyDown(Keys.ControlKey)) {
                        if (isKeyDown(Keys.ShiftKey) || isKeyDown(Keys.Alt)){
                            //Debug.Print("Listo para un shortcut");
                            onShortCutPressed(args);
                        }
                    }
                }

                bool isKeyPressed = ((ulong)lParam & 0x40000000) == 0x40000000;
                if (isKeyPressed){
                    onKeyPressed(args);
                }

                bool isLastKeyUp = ((ulong)lParam & 0x80000000) == 0x80000000;
                if (isLastKeyUp) {
                    onKeyReleased(args);
                }
                
               
            }
            return CallNextHookEx(hookID, ncode, wParam, lParam);
        }

        private bool isKeyDown(Keys keys){
            return (GetKeyState((int)keys) & 0x8000) == 0x8000;
        }



        ///////////////////EVENTS/////////////////////////
        protected virtual void onKeyDown(KeyEventArgs args){
            keyDown?.Invoke(this, args);
        }

        protected virtual void onKeyPressed(KeyEventArgs args)
        {
            keyPressed?.Invoke(this, args);
        }


        protected virtual void onKeyReleased(KeyEventArgs args){
            keyReleased?.Invoke(this, args);
        }

        protected virtual void onShortCutPressed(KeyEventArgs args){
            shortcutPressed?.Invoke(this, args);
        }

        protected virtual void onTextPasted(ClipBoardEventArgs args) {
            textPasted?.Invoke(this, args);
        }

        protected virtual void onTextSwapped(TextSwapEventArgs args) {
            textSwapped?.Invoke(this, args);
        }


    }



   /* public class InterceptKeys

    {

        public delegate int LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);



        private static LowLevelKeyboardProc_proc = HookCallback;

 

        private static IntPtr _hookID = IntPtr.Zero;



        privates tatic Microsoft.Office.Tools.CustomTaskPane ctpRef = null;



        //Declare the mouse hook constant. //For other hook types, you can obtain these values from Winuser.h in the Microsoft SDK.

        private const int WH_KEYBOARD = 2;

        private const int HC_ACTION = 0;

        public static void SetHook()

        {

            _hookID = SetWindowsHookEx(WH_KEYBOARD, _proc, IntPtr.Zero, (uint)AppDomain.GetCurrentThreadId());

        }

        public static void ReleaseHook()

        {

            UnhookWindowsHookEx(_hookID);

        }

        private static int HookCallback(int nCode, IntPtr wParam, IntPtr lParam)

        {

            int PreviousStateBit = 31;

            bool KeyWasAlreadyPressed = false;

            Int64 bitmask = (Int64)Math.Pow(2, (PreviousStateBit – 1));

            if (nCode < 0)

            {

                return (int)CallNextHookEx(_hookID, nCode, wParam, lParam);

            }

            else

            {

                if (nCode == HC_ACTION)

                {

                    Keys keyData = (Keys)wParam;

                    KeyWasAlreadyPressed = ((Int64)lParam & bitmask) > 0;

                    if (Functions.IsKeyDown(Keys.ControlKey) && keyData == Keys.D1 && KeyWasAlreadyPressed == false)

                    {

                        object missing = System.Reflection.Missing.Value;

                        Excel.

                        Workbook exBook = Excel_CustomTaskPane_ACC.Globals.ThisAddIn.Application.ActiveWorkbook;

                        Excel.

                        Worksheet exSheet = (Excel.Worksheet)exBook.ActiveSheet;

                        exSheet.get_Range(

“A4”, missing).Value2 = “A”;

                    }

                }

                return (int)CallNextHookEx(_hookID, nCode, wParam, lParam);

            }

        }



        [

        DllImport(“user32.dll”, CharSet = CharSet.Auto, SetLastError = true)]



        private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport(“user32.dll”, CharSet = CharSet.Auto, SetLastError = true)]

        [return: MarshalAs(UnmanagedType.Bool)]

        private static extern bool UnhookWindowsHookEx(IntPtrhhk);

        [DllImport(“user32.dll”, CharSet = CharSet.Auto, SetLastError = true)]

        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtrlParam);

    }

    public classFunctions

{

public static bool IsKeyDown(Keyskeys)

    {

        return (GetKeyState((int)keys) & 0x8000) == 0x8000;

    }

    [DllImport(“user32.dll”)]

    static extern short GetKeyState(intnVirtKey);

}*/

}
