﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapuccinoWorkBench
{
    /// <summary>
    /// Patatita para optimizar operaciones de texto, devuelve el segmento y su índice en el Array
    /// </summary>
    struct SegmentTrade
    {
        public Segment segment;
        public int index;
        public bool empty;

        public SegmentTrade(Segment seg, int ind){
            segment = seg;
            index = ind;
            empty = false;
        }
        public SegmentTrade(bool em) {
            this.empty = em;
            segment = null;
            index = -1;
        }
    }
}
