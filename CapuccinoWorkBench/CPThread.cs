﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CapuccinoWorkBench
{
    abstract class CPThread {

        //Threads
        ThreadStart _threadStart;
        Thread _thread;

        public bool isStarted;

        protected CPThread() {
            _threadStart = new ThreadStart(this.runThread);
            _thread = new Thread(_threadStart);
            isStarted = false;
        }

        public void start() {
            _thread.Start();
            isStarted = true;
        }
        public void join() {
            try { _thread.Join(); }
            catch (Exception) { }
        }
        public bool isAlive {
            get{ return _thread.IsAlive; }
        }

        public abstract void runThread();

    }
}
