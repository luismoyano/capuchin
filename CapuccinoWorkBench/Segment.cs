﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Segmento único de texto
/// Contiene la linea en la que existe y el contenido como tal
/// </summary>

namespace CapuccinoWorkBench
{
    class Segment
    {
        private int initPosition { get; set; }
        private int endPosition { get; set; }
        private StringBuilder originalContent { get; }

        private int processedInitPosition { get; set; }
        private int processedEndPosition { get; set; }
        private StringBuilder processedContent { get; set; }

        public CPSegstate state;
        public bool isOriginalDisplayed = true;//Al inicio asumimos esto como cierto


        

        public Segment(string content, int pos, int end) {
            initPosition = pos;
            endPosition = end;
            originalContent = new StringBuilder(content);
            processedContent = new StringBuilder();

            //Tener en cuenta para el secure text ops
            state = CPSegstate.NOTHING;
            isOriginalDisplayed = true;
        }

        private bool isEdited()
        {
            return state == CPSegstate.SAVE ||
                state == CPSegstate.COMMIT ||
                state == CPSegstate.EDITING;
        }

        public Point getCurrentRange(){
            if (!isOriginalDisplayed){
                return new Point(processedInitPosition, processedEndPosition);
            }
            return getOriginalRange();
        }

        public string getOriginal(){
            return originalContent.ToString();
            //return isEdited ? processedContent.ToString() : originalContent.ToString();
        }

        public Point getOriginalRange() {
            return new Point(initPosition, endPosition);
        }

        public void setProcessed(int init, int end, string content) {
            processedInitPosition = init;
            processedEndPosition = end;

            //En caso de que ya exista una propuesta
            processedContent.Clear();

            processedContent.Append(content);

            state = CPSegstate.SAVE;
        }

        internal void offsetRangeByBuffer(int buffer)
        {
            //Los originales siempre tienen offset
            initPosition += buffer;
            endPosition += buffer;

            if (isEdited()){
                processedInitPosition += buffer;
                processedEndPosition += buffer;
            }
        }

        

        public int getOffset(int len) {
            if (isEdited()) {

                if (len != -1){
                    return
                        isOriginalDisplayed ?
                            len - originalContent.ToString().Length :
                            len - processedContent.ToString().Length;
                }
                return
                    isOriginalDisplayed ?
                            originalContent.ToString().Length - processedContent.ToString().Length :
                            processedContent.ToString().Length - originalContent.ToString().Length;
            }
            return 0;
        }

        internal string getProcessed(){
            if (isEdited()) {
                return processedContent.ToString();
            }
            return "";
        }

        internal Point getProcessedRange(){
            if (isEdited()) {
                return new Point(processedInitPosition, processedEndPosition);
            }
            return new Point(-1, -1);
        }
    }
}
