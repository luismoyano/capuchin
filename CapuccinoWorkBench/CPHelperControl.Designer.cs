﻿namespace CapuccinoWorkBench
{
    partial class CPHelperControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.proposalsLabel = new System.Windows.Forms.Label();
            this.originLbl = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.originList = new System.Windows.Forms.DataGridView();
            this.proposalsList = new System.Windows.Forms.DataGridView();
            this.feedTable = new System.Windows.Forms.TableLayoutPanel();
            this.feedLabel = new System.Windows.Forms.Label();
            this.loadBar = new System.Windows.Forms.ProgressBar();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.originList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.proposalsList)).BeginInit();
            this.feedTable.SuspendLayout();
            this.SuspendLayout();
            // 
            // proposalsLabel
            // 
            this.proposalsLabel.AutoSize = true;
            this.proposalsLabel.Location = new System.Drawing.Point(3, 275);
            this.proposalsLabel.Name = "proposalsLabel";
            this.proposalsLabel.Size = new System.Drawing.Size(53, 13);
            this.proposalsLabel.TabIndex = 2;
            this.proposalsLabel.Text = "Proposals";
            // 
            // originLbl
            // 
            this.originLbl.AutoSize = true;
            this.originLbl.Location = new System.Drawing.Point(3, 0);
            this.originLbl.Name = "originLbl";
            this.originLbl.Size = new System.Drawing.Size(32, 13);
            this.originLbl.TabIndex = 0;
            this.originLbl.Text = "origin";
            this.originLbl.Click += new System.EventHandler(this.label1_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoScroll = true;
            this.tableLayoutPanel1.AutoScrollMargin = new System.Drawing.Size(3, 3);
            this.tableLayoutPanel1.AutoScrollMinSize = new System.Drawing.Size(1, 1);
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.originLbl, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.proposalsLabel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.originList, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.proposalsList, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.feedTable, 0, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.727273F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 91.27273F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 277F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(250, 600);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // originList
            // 
            this.originList.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.OldLace;
            this.originList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.originList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.originList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.originList.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.originList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.originList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.originList.DefaultCellStyle = dataGridViewCellStyle3;
            this.originList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.originList.Location = new System.Drawing.Point(3, 27);
            this.originList.Name = "originList";
            this.originList.RowHeadersVisible = false;
            this.originList.Size = new System.Drawing.Size(247, 245);
            this.originList.TabIndex = 3;
            this.originList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.originList_CellContentClick);
            // 
            // proposalsList
            // 
            this.proposalsList.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.proposalsList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.proposalsList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.proposalsList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.proposalsList.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.proposalsList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.proposalsList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.proposalsList.DefaultCellStyle = dataGridViewCellStyle6;
            this.proposalsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.proposalsList.Location = new System.Drawing.Point(3, 299);
            this.proposalsList.Name = "proposalsList";
            this.proposalsList.RowHeadersVisible = false;
            this.proposalsList.Size = new System.Drawing.Size(247, 271);
            this.proposalsList.TabIndex = 4;
            // 
            // feedTable
            // 
            this.feedTable.AutoSize = true;
            this.feedTable.ColumnCount = 2;
            this.feedTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.feedTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 171F));
            this.feedTable.Controls.Add(this.feedLabel, 0, 0);
            this.feedTable.Controls.Add(this.loadBar, 1, 0);
            this.feedTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.feedTable.Location = new System.Drawing.Point(3, 576);
            this.feedTable.Name = "feedTable";
            this.feedTable.RowCount = 1;
            this.feedTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.feedTable.Size = new System.Drawing.Size(247, 21);
            this.feedTable.TabIndex = 5;
            // 
            // feedLabel
            // 
            this.feedLabel.AutoSize = true;
            this.feedLabel.Location = new System.Drawing.Point(3, 0);
            this.feedLabel.Name = "feedLabel";
            this.feedLabel.Size = new System.Drawing.Size(35, 13);
            this.feedLabel.TabIndex = 0;
            this.feedLabel.Text = "label1";
            // 
            // loadBar
            // 
            this.loadBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loadBar.ForeColor = System.Drawing.Color.GreenYellow;
            this.loadBar.Location = new System.Drawing.Point(79, 3);
            this.loadBar.Name = "loadBar";
            this.loadBar.Size = new System.Drawing.Size(165, 15);
            this.loadBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.loadBar.TabIndex = 1;
            this.loadBar.Visible = false;
            // 
            // CPHelperControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "CPHelperControl";
            this.Size = new System.Drawing.Size(250, 600);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.originList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.proposalsList)).EndInit();
            this.feedTable.ResumeLayout(false);
            this.feedTable.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label proposalsLabel;
        private System.Windows.Forms.Label originLbl;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.DataGridView originList;
        public System.Windows.Forms.DataGridView proposalsList;
        private System.Windows.Forms.TableLayoutPanel feedTable;
        public System.Windows.Forms.Label feedLabel;
        public System.Windows.Forms.ProgressBar loadBar;
    }
}
