﻿using System;
using System.Data;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Drawing;

using System.Diagnostics;//Print para debuggear, Eliminar para el pack final
using System.Runtime.InteropServices;
using Microsoft.Office.Tools.Word;

namespace CapuccinoWorkBench
{


    public partial class ThisAddIn
    {
        //Error strings
        private const String noSessionFound = "No Capuchin Logged";
        private const String noPropFound = "No available proposal";
        private const String fileExists = "There is already a db with that name, please enter another name";
        
        private const String sessionOn = "There is a capuchin file already loaded, please commit your changes and close the database before creating or loading a new one";
        private const String sqlError = "There seems to be an SQL Error, please load another Database";

        private const string cpError = "There seems to be an error with the .capuchin file, please load another one";

        //Status strings
        private const String loading = "Loading... Please don't modify the text.";
        private const string cpOn = "Capuchin logged";

        //Swapping strings
        private const string editSwapIn = "[*";
        private const string editSwapOut = "/]";

        

        //Helper panel
        public CPHelperControl helperControl;
        private CustomTaskPane helperPanel;
        private const String helperName = "Units Vision Panel";
        private const String colHeader = "Content";

        //Getter del panel
        public CustomTaskPane panel { get { return helperPanel; } }


        #region debug stuff

        //Consola de debug
        private const bool DEBUGSTUFF = false;
        private static DebugLog log;
        delegate void printTextCallback(string text);

        
        #endregion

        ////////////////////MODEL/////////////////////////////////////
        //Managers
        private CPDataBaseHandler dbHandler;
        private IDocumentManager docManager;
        private ITextManager textManager;

        private CPInputListener input;
        private CPLogIn logIn;

        private CPShortcuts shortcuts;

        //Threads
        CPCursorThread cursorThread;
        delegate void OriginEntryThreadManager(String[] entries);//, String[] props);

        CPCursorThread editThread;

        //Gatherer
        BackgroundWorker loadWorker;//Worker para el load inicial que es costoso de la ostia

        //cursorVars
        private Point lastCursorSituation;
        private Point currentCursorSituation;

        private int editCursor;
        private int lastSt;//start anterior
        private int lastEn;//end anterior
        
        //highlighters
        private Point lastHighlighted;

        

        private Point currentHighlighted;
        private CPSegstate prevCode;

        //Command bar
        ContextMenuStrip propsStrip;


        private void ThisAddIn_Startup(object sender, EventArgs e)
        {
            if (DEBUGSTUFF) {
                log = new DebugLog();
                log.Show();
                print("Empiezo debugger capuchin");
            }


            //Inicializo panel lateral para todas las ventanas abiertas
            AddAllTaskPanes();

            //Inicio las listas
            //helperControl.originList.ColumnCount = 1;
            



            dbHandler = new CPDataBaseHandler();
            print("creo dbHandler");


            textManager = new CPTextManager();

            print("creo text manager");


            docManager = new CPDocumentManager();

            print("Creo el doc manager");
            

            input = new CPInputListener();
            input.keyDown += onKeyDown;
            input.keyReleased += onKeyUp;
            input.keyPressed += onKeyPressed;
            input.shortcutPressed += onShortCutPressed;

            print("creo procesador de teclado");
            

            //DBLoader
            loadWorker = new BackgroundWorker();
            loadWorker.WorkerSupportsCancellation = false;
            loadWorker.WorkerReportsProgress = true;
            loadWorker.DoWork += loadText;
            loadWorker.RunWorkerCompleted += loadComplete;



            //Inicio Threads
            initThreads();
            

            //Creo el context menu
            propsStrip = new ContextMenuStrip();

            //Creo el diálogo de login
            logIn = new CPLogIn();

            //Creo clase de shortcuts
            shortcuts = new CPShortcuts();


            //Eventos para trackear la creacíón de nuevos documentos
            Application.DocumentOpen += Application_DocumentOpen;

            Application.DocumentChange += Application_DocumentChange;

            Word.ApplicationEvents2_Event wdEvents2 = (Word.ApplicationEvents2_Event)this.Application;
            wdEvents2.NewDocument += new Word.ApplicationEvents2_NewDocumentEventHandler(Application_NewDocument);
        }

        

        private void ThisAddIn_Shutdown(object sender, EventArgs e)
        {
            dbHandler.closeDB();

            if (cursorThread != null){
                cursorThread.stopLooping();
                cursorThread.join();
            }

            input.stopListeningAll();
            protectText(false);

            print("cierro el programa");
            
            
        }


        private void initThreads()
        {
            cursorThread = new CPCursorThread(0.5f, updateCurrentTranslationUnit);
            print("Creo un thread secundario");

            editThread = new CPCursorThread(0.3f, keepEditInCheck);
        }

        private void Application_DocumentChange(){
            Debug.Print("Document Change");

            RemoveOrphanedTaskPanes();
        }

        private void Application_NewDocument(Word.Document Doc){
            Debug.Print("New Document");

            AddTaskPane(Doc);
        }

        private void Application_DocumentOpen(Word.Document Doc){
            Debug.Print("Document Open");

            RemoveOrphanedTaskPanes();

            AddTaskPane(Doc);
        }
        
        

        public void AddAllTaskPanes() {
            if (Globals.ThisAddIn.Application.Documents.Count > 0){
                if (this.Application.ShowWindowsInTaskbar == true){
                    foreach (Word.Document doc in this.Application.Documents){
                        AddTaskPane(doc);
                    }
                }
                else AddTaskPane(this.Application.ActiveDocument); 
            }
        }

        private void AddTaskPane(Word.Document doc){

            //if (helperPanel != null && helperPanel.Window == doc.ActiveWindow) return;

             
            helperPanel = this.CustomTaskPanes.Add(new CPHelperControl(), helperName, doc.ActiveWindow);
            helperPanel.Visible = true;
            //helperPanel.VisibleChanged += new EventHandler(togglePanelVisibility);

            ((CPHelperControl)helperPanel.Control).originList.Columns.Add(colHeader, colHeader);
            ((CPHelperControl)helperPanel.Control).proposalsList.Columns.Add(colHeader, colHeader);

            ((CPHelperControl)helperPanel.Control).proposalsList.KeyUp += onProposalSelected;

            ((CPHelperControl)helperPanel.Control).feedLabel.Text = noSessionFound;

            print("Creo panel lateral");

        }

        private void RemoveOrphanedTaskPanes(){
            for (int i = this.CustomTaskPanes.Count; i > 0; i--){
                CustomTaskPane pane = this.CustomTaskPanes[i - 1];
                if (pane.Window == null){
                    this.CustomTaskPanes.Remove(pane);
                }
            }
        }

        public CustomTaskPane getSelfPanel() {
            foreach (CustomTaskPane pane in Globals.ThisAddIn.CustomTaskPanes){
                if (pane.Window == Globals.ThisAddIn.Application.ActiveDocument.ActiveWindow){
                    //Debug.Print("He encontrado un panel que pertenece a esta ventana");
                    //Es mi ventana
                    if (pane.Control.GetType() == typeof(CPHelperControl)){
                        //Esta es la que buscamos
                        //Debug.Print("He encontrado mi panel");

                        return pane;
                    }
                }
            }
            return null;
        }

        private void togglePanelVisibility(object sender, EventArgs e) {
            //Pongo el panel o lo quito...
            Debug.Print("Hago el panel visible e invisible");
        }

        public void getWordText() {//Va muy bien
            //print("Empiezo gather de texto");

            int start = Application.ActiveDocument.Content.Start;
            int end = Application.ActiveDocument.Content.End;
            if (!((CPTextManager)textManager).hasGatheredText()){
                ((CPTextManager)textManager).gatherText(new StringBuilder(Application.ActiveDocument.Range(start, end).Text));
            }
            //print("Termino gather de texto");
        }

#region Get Selections
        public int getCursorPosition()
        {
            try
            {
                if (Application.Selection != null)
                {
                    
                    //Debug.Print(Application.Selection.Start.ToString());
                    return Application.Selection.Start;

                }
            }
            catch (Exception e) { print("Error en get cursor position"); }
            return 0;
        }

        public Point getCursorSelection(bool refresh = false){
            if (Application.Selection != null)
            {
                
                if (refresh) { Application.Selection.Calculate(); }

                Point selection = new Point(Application.Selection.Start, Application.Selection.End);
                //Debug.Print(selection.ToString());
                return selection;
            }
            return new Point();
        } 
#endregion


#region Update Views
        //threadCalled
        public Point updateCurrentTranslationUnit()
        {
            //Obtengo la posición del cursor
            //aviso al text manager para que haga lo suyo

            currentCursorSituation = getCursorSelection();

            String[] currentUnit = textManager.getUnitsByRange(getCursorSelection());

            //Debug.Print("current unit = " + currentUnit.Length);

            //UpdateViews
            safeUpdateViews(currentUnit);


            lastCursorSituation = currentCursorSituation;
            return currentCursorSituation;
        }


        private void safeUpdateViews(String[] originals)
        {//){Thread Safe (:

            //Debug.Print("Intento update de Views");
            CPHelperControl control;
            try { control = (CPHelperControl)getSelfPanel().Control; }
            catch (Exception e) { return; }

            if (control.InvokeRequired)
            {//No podemos escribir en los listviews

                //Debug.Print("No puedo hacer safe update");

                OriginEntryThreadManager entryManager = new OriginEntryThreadManager(safeUpdateViews);
                control.Invoke(entryManager, new object[] { originals });
            }
            else
            {//Podemos escribir en los views

               //Debug.Print("Hago thread safe update de views");

                ////////////////////HIGHLIGHT/////////////////////
                try {
                    currentHighlighted = textManager.getUnitRangeByCursor(getCursorPosition());

                    if (currentHighlighted != lastHighlighted){

                        highlightText(currentHighlighted,
                            textManager.getUnitState(getCursorPosition())
                           );

                        highlightText(lastHighlighted, CPSegstate.CLEAR);
                    }

                   lastHighlighted = currentHighlighted;

                }
                catch (Exception e){
                    Debug.Print("Error al resaltar");
                    Debug.Print(e.StackTrace);
                }
           



                ////////////////LISTVIEWS////////////////////////
                if (currentCursorSituation != lastCursorSituation){

                    updateListViewsByOriginals(originals);
                    
                }

                
                /////////////TEXTOPS///////////////////////
                secureOperations();
            }
        }


        //////////////////LISTVIEWS////////////////////

        private void updateListViews(string[] originals, string[] props)
        {
            CPHelperControl control;
            try { control = (CPHelperControl)getSelfPanel().Control; }
            catch (Exception e) { return; }

            control.originList.Rows.Clear();
            control.proposalsList.Rows.Clear();

            if (dbHandler.doesDBExist())
            {//Si hay alguna base de datos
                if (originals != null)
                {
                    for (int i = 0; i < originals.Length; i++)
                    {
                        control.originList.Rows.Add(originals[i]);
                    }
                }
            }


            //Escribimos en props

            if (dbHandler.doesDBExist())
            {//Si hay alguna base de datos

                if (props != null && props.Length > 0)
                {
                    for (int i = 0; i < props.Length; i++)
                    {
                        control.proposalsList.Rows.Add(props[i]);
                    }
                }
                else { control.proposalsList.Rows.Add(noPropFound);/*No hay propuestas para esa unidad*/}

            }
        }

        private void wakeUpListViews()
        {

            getWordText();

            //Update al crear una base de datos
            safeUpdateViews(textManager.getUnitsByRange(getCursorSelection()));
            
        }

        private void updateListViews()
        {
            String[] currentUnit = textManager.getUnitsByRange(getCursorSelection());
            updateListViewsByOriginals(currentUnit);
        }

        /// <summary>
        /// Permite updatear los rows de las dos listas basándose en el origen, tal que para cada row origen se vea una propuesta y además si un row de origen tiene más de una propuesta se pueda ver por color aún así.
        /// </summary>
        /// <param name="originals">Lista de órígenes a procesar</param>
        private void updateListViewsByOriginals(string[] originals)
        {
            CPHelperControl control;
            try { control = (CPHelperControl)getSelfPanel().Control; }
            catch (Exception e) { return; }

            control.originList.Rows.Clear();
            control.proposalsList.Rows.Clear();

            if (dbHandler.doesDBExist())
            {//Si hay alguna base de datos
                if (originals != null)
                {
                    for (int i = 0; i < originals.Length; i++)
                    {
                        control.originList.Rows.Add(originals[i]);
                        control.originList.Rows[i].DefaultCellStyle.BackColor = i % 2 == 0 ? Color.White : Color.OldLace;

                        string[] props = dbHandler.retrieveProposalsByOrigin(originals[i]);

                        if (props != null && props.Length > 0){

                            for (int j = 0; j < props.Length; j++){
                                
                                control.proposalsList.Rows.Add(props[j]);
                                control.proposalsList.Rows[j].DefaultCellStyle.BackColor = i % 2 == 0 ? Color.White : Color.OldLace;

                            }
                        }
                        else{
                            control.proposalsList.Rows.Add(noPropFound);
                            control.proposalsList.Rows[i].DefaultCellStyle.BackColor = i % 2 == 0 ? Color.White : Color.OldLace;
                        }
                    }
                }
            }

        }

        private void protectText(bool protect = true){
            try{
                if (protect)
                {
                    if (Application.ActiveDocument.ProtectionType != Word.WdProtectionType.wdAllowOnlyReading)
                    {
                        Application.ActiveDocument.Protect(Word.WdProtectionType.wdAllowOnlyReading);
                    }
                }
                else if (Application.ActiveDocument.ProtectionType != Microsoft.Office.Interop.Word.WdProtectionType.wdNoProtection) {
                    Application.ActiveDocument.Unprotect();
                }
            }
            catch (Exception) { print("Excepción en protext text"); }
        }
        #endregion


        #region AsyncGather
        ///////////////////////////ASYNCGATHER//////////////////////////////

        private void beginAsyncGather(string cpPath = "")
        {
            if (!loadWorker.IsBusy){
                ((CPHelperControl)helperPanel.Control).feedLabel.Text = loading;
                loadWorker.RunWorkerAsync(cpPath);
                
            }
        }


        private void loadText(object sender, DoWorkEventArgs e){

            string loadPath = (string)e.Argument;

            if (loadPath == ""){
                getWordText();
            }
            else{
                //leer desde el docmanager
                e.Result = docManager.loadCapuchinFile(loadPath, out ((CPTextManager)textManager).inputText.segmentedText);//Testear

            }
        }

        private void loadComplete(object sender, RunWorkerCompletedEventArgs e){

            if (e.Result != null)
            {
                switch ((CapuchinFileResultCode)e.Result)
                {
                    case CapuchinFileResultCode.ALREADYLOADED:
                        MessageBox.Show(sessionOn);
                        saveSession();
                        break;
                    case CapuchinFileResultCode.BADFILE:
                        MessageBox.Show(cpError);
                        saveSession();
                        break;
                    case CapuchinFileResultCode.SUCCESS:
                        postLoadSuccess();
                        
                        break;
                }
            }
            else {
                postLoadSuccess();
            }

            //Reinicio los threads
            initThreads();

            cursorThread.start();
            //print("Empiezo watchdog");
            
        }
        private void postLoadSuccess() {
            docManager.createDocCopy(Application.ActiveDocument.FullName);
            
            logIn.notifyResult(CPLogInResultCode.SUCCESS);

            //Escribo desde otro thread
            ((CPHelperControl)helperPanel.Control).Invoke(new System.Action(() =>
            {
                ((CPHelperControl)helperPanel.Control).feedLabel.Text = cpOn;
            }));
            //((CPHelperControl)helperPanel.Control).feedLabel.Text = cpOn;
        }

        private void showPropsByMenuStrip() {

            CPHelperControl control;
            try { control = (CPHelperControl)getSelfPanel().Control; }
            catch (Exception e) { return; }

            //Borro lo que hubiese de antes en el propsStrip
            propsStrip.Items.Clear();

            //Agarro las props del panel lateral
            //Pueblo el context menu
            for (int i = 0; i < control.proposalsList.Rows.Count; i++){
                propsStrip.Items.Add(
                    control.proposalsList.Rows[i].ToString()
                    );
            }

            //Busco la posición para ponerlo
            //TODO

            //Le pongo el foco
            propsStrip.Focus();
        }

        public void focusToPropsList() {

            CPHelperControl control;
            try { control = (CPHelperControl)getSelfPanel().Control; }
            catch (Exception e) { return; }

            control.proposalsList.Focus();
        }

        private void focusToWordDoc(){
            Application.ActiveWindow.SetFocus();
        }

        #endregion


        #region DataBase Commands
        ///////////////////DATABASE COMMANDS////////////////////////////
        private String[] retrieveDBEntries(String[] entries)
        {
            List<string> retrieved = new List<string>();
            if (entries != null)
            {
                for (int i = 0; i < entries.Length; i++)
                {
                    String[] props = dbHandler.retrieveProposalsByOrigin(entries[i]);
                    if (props != null) { retrieved.AddRange(props); }
                }
            }
            return retrieved.ToArray();
        }

        internal void createNewDataBaseFile()
        {
            //Protejo el texto
            protectText();

            //Create new window 
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            DialogResult answer = dialog.ShowDialog();


            if (answer == DialogResult.OK)
            {
                String dbPath = dialog.SelectedPath;


                GeneralPrompt namePrompt = new GeneralPrompt();
                DialogResult result = namePrompt.ShowDialog();

                if (result == DialogResult.OK)
                {

                    dbPath += "\\" + namePrompt.resultName;

                    print("Creo una base de datos aquí = " + dbPath);


                    if (!File.Exists(dbPath))
                    {
                        CPDataBaseResultCode res = dbHandler.createNewEmptyDataBase(dbPath);

                        switch (res)
                        {
                            case CPDataBaseResultCode.SUCCESS:
                                dbHandler.createCapuchinTables();
                                //if(gather) beginAsyncGather();
                                dbHandler.loaded = true;
                                break;

                            case CPDataBaseResultCode.DBALREADYLOADED:
                                //Error Dialog
                                MessageBox.Show(sessionOn);
                                saveSession();
                                break;
                        }
                    }
                    else{MessageBox.Show(fileExists);}
                }
            }
        }

        internal void loadDBFile(string path)
        {//funciona

            //Protejo el texto
            protectText();
            
            CPDataBaseResultCode res = dbHandler.loadExistingDataBase(path);

            switch (res)
            {
                case CPDataBaseResultCode.SUCCESS:
                    //print("La carga se ha hecho bien");
                    dbHandler.loaded = true;
                    break;
                case CPDataBaseResultCode.SQLERROR:
                case CPDataBaseResultCode.NOTADATABASE:
                    MessageBox.Show(sqlError);
                    break;
                case CPDataBaseResultCode.DBALREADYLOADED:
                    MessageBox.Show(sessionOn);
                    break;

            }
        }

        internal void saveTranslationByUnit()
        {
            string unit = textManager.getUnitByCursor(getCursorPosition());
            string prop = textManager.getProposalByUnit(getCursorPosition());

            dbHandler.addOriginAndProposal(unit, prop);

            updateListViews();
        } 
#endregion


        private void highlightText(Point rng, CPSegstate code) {
            protectText(false);
            switch (code) {
                case CPSegstate.NOTHING:
                    Application.ActiveDocument.Range(rng.X, rng.Y).Font.Shading.BackgroundPatternColorIndex = Word.WdColorIndex.wdYellow;
                    protectText();
                    break;
                case CPSegstate.EDITING:
                    Application.ActiveDocument.Range(rng.X, rng.Y).Font.Shading.BackgroundPatternColorIndex = Word.WdColorIndex.wdTurquoise;
                    break;
                case CPSegstate.SAVE:
                    Application.ActiveDocument.Range(rng.X, rng.Y).Font.Shading.BackgroundPatternColorIndex = Word.WdColorIndex.wdViolet;
                    break;
                case CPSegstate.COMMIT:
                    Application.ActiveDocument.Range(rng.X, rng.Y).Font.Shading.BackgroundPatternColorIndex = Word.WdColorIndex.wdBrightGreen;
                    break;
                default:
                    Application.ActiveDocument.Range(rng.X, rng.Y).Font.Shading.BackgroundPatternColorIndex = Word.WdColorIndex.wdNoHighlight;
                    protectText();
                    break;
            }

            prevCode = code;
        }



#region Text Operations
        //////////////////////////TEXT OPERATIONS///////////////////////////


        /// <summary>
        /// Utilizando el rango de texto y a partir de ciertos criterios, los botones para hacer una u otra operación se desactivarán según cada situación
        /// haciendo que las operaciones sólo se puedan efectuar cuando sea 'seguro'
        /// </summary>
        private void secureOperations()
        {
            /**
             * Begin session: Sólo se activa cuando no hay sesión activa. OK
             * Save session: Se activa cuando hay sesión activa. OK
             * 
             * Edit mode: Cuando la unidad actual no esté en editing. OK
             * Save edit: Cuando la unidad actual esté en editing. OK
             * 
             * Select from list: Si hay sesión activa OK
             * 
             * Save proposal to DB: Siempre y cuando la unidad esté en save o commit (igual que swap). OK
             * 
             * Show panel: SIEMPRE
             * Shortcuts: SIEMPRE
             * About: SIEMPRE
             * 
             * Crear: Sólo se puede en rango y cuando no se esté pisando una unidad existente Y no está en editing. OK
             * Ignore: Se puede hacer sobre unidad o rango, puede ser no consecutivo Y no está en editing. OK
             * Join: Sólo dos consecutivos Y no está en editing. OK
             * Split: Sólo se puede hacer unidad existente Y no está en editing. OK
             * Swap: Siempre y cuando la unidad esté en save o commit. OK
             * **/

            try
            {
                Globals.Ribbons.CPRibbon.disableAllButtons();

                if (logIn.isGameOn)
                {
                    Point range = getCursorSelection();
                    CPSegstate state = textManager.getUnitState(range.X);
                    //Debug.Print("Reviso los botones");

                    if (state != CPSegstate.EDITING)
                    {
                        if (range.X < range.Y)
                        {
                            //Podemos activar operaciones de rango (create y Join)

                            Point[] ranges = textManager.getUnitsSpanByRange(range);
                            if (ranges.Length <= 0)
                            {
                                Globals.Ribbons.CPRibbon.createUnitBtn.Enabled = true;
                            }
                            else
                            {
                                //Hay algo y es borrable

                                Globals.Ribbons.CPRibbon.ignoreUnitBtn.Enabled = true;

                                //Joins
                                if (ranges.Length > 1 && ranges.Length < 3)
                                {//Podemos hacer Join
                                    Globals.Ribbons.CPRibbon.joinUnitsBtn.Enabled = true;
                                }

                            }
                        }

                        if (range.X == range.Y)
                        {//Si sólo hay cursor y no selección
                         //Si el cursor está dentro de una unidad y no en sus extremos
                            Point unitRng = textManager.getUnitRangeByCursor(range.X);
                            if (unitRng != new Point())
                            {//Podemos borrar y a lo mejor hacer split
                                Globals.Ribbons.CPRibbon.ignoreUnitBtn.Enabled = true;
                                if (range.X >= unitRng.X + 1 && range.X <= unitRng.Y - 1)
                                {
                                    //podemos activar split
                                    Globals.Ribbons.CPRibbon.splitUnitsBtn.Enabled = true;
                                }
                            }
                        }

                        Globals.Ribbons.CPRibbon.EditModebtn.Enabled = true;

                        if (state == CPSegstate.COMMIT || state == CPSegstate.SAVE){
                            //Globals.Ribbons.CPRibbon.swapBtn.Enabled = true;
                            Globals.Ribbons.CPRibbon.saveUnitToDbBtn.Enabled = true;
                        }
                    }
                    else {
                        Globals.Ribbons.CPRibbon.saveEditBtn.Enabled = true;
                    }

                    Globals.Ribbons.CPRibbon.focusPropsBtn.Enabled = true;
                    Globals.Ribbons.CPRibbon.saveBtn.Enabled = true;
                }
                else {
                    Globals.Ribbons.CPRibbon.beginBtn.Enabled = true;
                }

            }
        
            catch (Exception e)
            {
                Debug.Print("EXCEPTION SECURE TEXT OPS");
                Debug.Print(e.StackTrace.ToString());
            }
            

        }

        internal void createUnit()
        {
            if (Globals.Ribbons.CPRibbon.createUnitBtn.Enabled)
            {
                if (Application.Selection != null)
                {
                    int start = Application.Selection.Start;
                    int end = Application.Selection.End;

                    bool success = false;

                    if (start < end)
                    {//Puede ser
                        Point rng = new Point(start, end);
                        String[] units = textManager.getUnitsByRange(rng);
                        if (units == null || units.Length <= 0)
                        {//Si que puede ser! :D
                            success = textManager.createUnitBySelection(rng, Application.Selection.Text);
                        }
                    }
                    if (!success) { /*Aquí algo va mal*/}
                    else { updateListViews(); }
                }
            }
        }

        internal void deleteUnit()
        {
            if (Globals.Ribbons.CPRibbon.ignoreUnitBtn.Enabled)
            {
                if (Application.Selection != null)
                {

                    int start = Application.Selection.Start;
                    int end = Application.Selection.End;

                    bool success = false;

                    if (start >= end) { success = textManager.deleteUnitByCursor(getCursorPosition()); }
                    else { success = textManager.deleteUnitBySelection(new Point(start, end)); }

                    if (!success) { /*Aquí algo va mal*/}
                    else { updateListViews(); }
                }
            }
        }

        internal void joinUnits()
        {
            if (Globals.Ribbons.CPRibbon.joinUnitsBtn.Enabled)
            {
                if (Application.Selection != null)
                {
                    bool success = false;
                    int start = Application.Selection.Start;
                    int end = Application.Selection.End;


                    if (start < end)
                    {//Puede ser
                        Point rng = new Point(start, end);
                        String[] units = textManager.getUnitsByRange(rng);
                        if (units.Length > 1)
                        {//Si que puede ser! :D
                            success = textManager.joinUnitsByRange(rng);
                        }
                    }
                    if (!success) { /*Aquí algo va mal*/}
                    else { updateListViews(); }
                }
            }
        }

        internal void splitUnit()
        {
            if (Globals.Ribbons.CPRibbon.splitUnitsBtn.Enabled)
            {
                if (Application.Selection != null)
                {
                    bool success = false;
                    int start = Application.Selection.Start;
                    int end = Application.Selection.End;


                    if (start == end)
                    {//Puede ser
                        success = textManager.splitUnit(getCursorPosition());
                    }
                    if (!success) { /*Aquí algo va mal*/}
                    else { updateListViews(); }
                }
            }
        }
#endregion


#region Input Listeners

        //Input listening
        private void onKeyDown(object sender, KeyEventArgs e){}

        private void onKeyPressed(object sender, KeyEventArgs e){}

        private void onKeyUp(object sender, KeyEventArgs e){}

        private void onShortCutPressed(object sender, KeyEventArgs e){

            Debug.Print(shortcuts.getCommand(e.KeyCode).ToString());
            
            switch (shortcuts.getCommand(e.KeyCode))
            {
                case CPCommands.Begin://begin
                    startCapuchinSession();
                    break;
                case CPCommands.SaveExit://saveexit
                    saveSession();
                    break;
                case CPCommands.EditUnit://edit
                    editUnit();
                    break;
                case CPCommands.SaveEdit://Save edit
                    saveEdit();
                    break;
                case CPCommands.Swap://swap
                    swapUnitNProposal();
                    break;
                case CPCommands.CreateUnit://Creat unit
                    createUnit();
                    break;
                case CPCommands.DeleteUnit://Ignore unit
                    deleteUnit();
                    break;
                case CPCommands.SplitUnit://Split unit
                    splitUnit();
                    break;
                case CPCommands.JoinUnit://join unit
                    joinUnits();
                    break;
                case CPCommands.FocusToProps://Select from list
                    focusToPropsList();
                    break;
                case CPCommands.CommitEdit:
                    commitEdit();
                    break;
                case CPCommands.AddText:
                    //Crear texto nuevo
                    break;
                case CPCommands.RemoveText:
                    //Borrar texto
                    break;
            }
        }

        internal void displayShortcutsView(){
            shortcuts.display();
        }


        #endregion

        #region unit Edition

        internal void editUnit(string edit = null){

            if (!Globals.Ribbons.CPRibbon.EditModebtn.Enabled) return;
            
            //Desactivo el botón de edit mode
            Globals.Ribbons.CPRibbon.EditModebtn.Enabled = false;
            Globals.Ribbons.CPRibbon.saveEditBtn.Enabled = true;

            //Libero el texto para editar.
            protectText(false);

            //Pauso el thread de interactividad
            cursorThread.pause();

            //Reemplazo texto por proposal wrapper (Esto a lo mejor no hace falta)
            putPropsHolder();

            //Stackeo la unidad que se va a editar
            textManager.stackCurrentUnit(getCursorSelection(true).X);

            if (edit != null){
                //Nos traemos el edit de casa
                replaceText(edit);
            }

            //Resumo el thread de manipulacion de edicion
            if (editThread.isStarted) { editThread.resume(); }
            else { editThread.start(); }

            
            
        }

        private Point getPropsHolderIndices() {

            int start = Application.ActiveDocument.Content.Start;
            int end = Application.ActiveDocument.Content.End;


            return new Point(
                Application.ActiveDocument.Range(start, end).Text.IndexOf(editSwapIn),
                Application.ActiveDocument.Range(start, end).Text.IndexOf(editSwapOut)
                );
        }

        private void replaceText(string edit) {

            Point holder = getPropsHolderIndices();

            Application.ActiveDocument.Range(holder.X + editSwapIn.Length, holder.Y).Delete();

            Application.ActiveDocument.Range(holder.X, holder.X + editSwapIn.Length).InsertAfter(edit);
        }


        private void putPropsHolder()
        {
            Point unitSpan = textManager.getUnitRangeByCursor(getCursorSelection(true).X);
            //Point prev = textManager.getFormerBlockSpan(getCursorSelection(true).X);
            //Application.ActiveDocument.Range(unitSpan.X, unitSpan.Y).Delete();

            Application.ActiveDocument.Range(unitSpan.X, unitSpan.X).InsertAfter(editSwapIn);
            Application.ActiveDocument.Range(unitSpan.Y + editSwapOut.Length, unitSpan.Y + editSwapOut.Length).InsertAfter(editSwapOut);

            Point holder = getPropsHolderIndices();

            //Setteo el edit cursor
            editCursor = holder.Y;


            //Highlight para edición
            highlightText(new Point(holder.X, holder.Y + editSwapOut.Length), CPSegstate.EDITING);
        }


        private Point keepEditInCheck(){
            //print("Mantengo el cursor en su posición");
            try{

                Point holder = getPropsHolderIndices();
                
                if (!(holder.X == -1)){
                    if (!(holder.Y == -1))
                    {
                        int cur = getCursorSelection(true).X;

                        if (!(cur >= holder.X + editSwapIn.Length && cur <= holder.Y + editSwapOut.Length))
                        {
                            Application.Selection.Start = holder.X + editSwapIn.Length;
                            Application.Selection.End = holder.Y + editSwapIn.Length;

                            
                        }
                    }
                    else {
                        //print("END NO SE ENCUENTRA");
                        Application.ActiveDocument.Range(lastEn, lastEn + 1).Delete();
                        Application.ActiveDocument.Range(lastEn - 1, lastEn).InsertAfter(editSwapOut);
                    }
                }
                else {
                    //Start no se encuentra, lo recolocamos
                    Application.ActiveDocument.Range(lastSt, lastSt + 1).Delete();
                    Application.ActiveDocument.Range(lastSt-1, lastSt).InsertAfter(editSwapIn);
                    
                }

                lastSt = holder.X == -1? lastSt : holder.X;
                lastEn = holder.Y == -1? lastEn : holder.Y;
            }
            catch(Exception e){
                Debug.Print("Excepcion en keepEditIncheck");
                Debug.Print(e.StackTrace);
            }
            

            return new Point (0, 0);
        }


        //Se salva el edit temporalmente
        public void saveEdit(){

            //Removemos el holder
            string newTxt = removePropsHolder();

            //Seteamos el segment
            textManager.saveProposalInCurrentUnit(newTxt);

            //Pausamos el thread de edición
            editThread.pause();

            //Resumimos el thread de búsqueda
            cursorThread.resume();

            //Protegemos el documento
            protectText(true);

            //Rehabilitamos el botón de edit
            Globals.Ribbons.CPRibbon.EditModebtn.Enabled = true;

            //Me deshabilito yo
            Globals.Ribbons.CPRibbon.saveEditBtn.Enabled = false;
        }

        public void swapUnitNProposal() {
            //Desprotejo el documento
            protectText(false);

            //Pauso el thread de cursor
            cursorThread.pause();

            //Swappeo
            string swappable = textManager.getCurrentOpposite(getCursorSelection(true).X);
            //print("Cojo el opuesto = " + swappable);
            if (swappable != "")
            {
                Point rng = textManager.getUnitRangeByCursor(getCursorSelection(true).X);
                Application.ActiveDocument.Range(rng.X, rng.Y).Delete();
                Application.ActiveDocument.Range(rng.X, rng.X).InsertAfter(swappable);

                textManager.swapUnitNProp(getCursorSelection(true).X);
                //Resalto a nada POR AHORA
                //TODO
            }
            //Resumo el thread
            cursorThread.resume();

            //Protejo el texto de nuevo
            protectText(true);
        }
        

        private string removePropsHolder(){

            int start = Application.ActiveDocument.Content.Start;
            int end = Application.ActiveDocument.Content.End;

            int st = Application.ActiveDocument.Range(start, end).Text.IndexOf(editSwapIn);
            int en = Application.ActiveDocument.Range(start, end).Text.IndexOf(editSwapOut);

            string newProp = Application.ActiveDocument.Range(st, en + editSwapOut.Length).Text;
            print(newProp);

            newProp = newProp.Replace(editSwapIn, "");

            print(newProp);

            newProp = newProp.Replace(editSwapOut, "");
            

            print(newProp);

            //Resaltamos el texto
            highlightText(new Point(st + editSwapIn.Length, en + editSwapOut.Length), CPSegstate.SAVE);

            Application.ActiveDocument.Range(st, en + editSwapOut.Length).Delete();

            Application.ActiveDocument.Range(st, st).InsertAfter(newProp + " ");//Fix para el espacio que se traga

            //Fix para el espacio final que se traga


            return newProp;
        }


        //Se guarda el edit en la base de datos
        public void commitEdit() {
            //Obtengo el string original
            string original = textManager.getUnitByCursor(getCursorSelection().X);

            //Obtengo el prop
            string committable = textManager.getProposalByUnit(getCursorSelection().X);

            //Los guardo en la base de datos
            if (committable != "") {
                dbHandler.addOriginAndProposal(original, committable);
                textManager.notifyCommit(getCursorSelection().X);
            }
        }

        private void onProposalSelected(object sender, KeyEventArgs e){
            if (e.KeyCode == Keys.Enter) {

                int cells = ((DataGridView)sender).SelectedCells.Count;

                if (cells == 1) {//Sólo se vale escoger un cell, ni 0 ni más de uno
                    int index = ((DataGridView)sender).SelectedCells[0].RowIndex -1;

                    string prop = ((DataGridView)sender).Rows[index].Cells[0].Value.ToString();

                    if (prop != "" && prop != noPropFound && prop != null){
                        editUnit(prop);
                        focusToWordDoc();
                    }
                }
            }
        }


        #endregion

        #region About screen
        //About
        internal void showAbout()
        {
            AboutApe about = new AboutApe();
            about.Show();
        }
        #endregion



        #region debugMethods
        public static void print(string text) {
            if (DEBUGSTUFF) {
                log.txt.AppendText("\r\n" + text);
            }

            Debug.Print(text);
        }
        #endregion

        #region session protocol


        internal void startCapuchinSession() { if (!logIn.isGameOn) { logIn.startLogInProcess(); } }

        internal void beginSessionWithParams(string databasePath, string cpfilePath){

            //Empezamos con el fichero capuchin
            if (cpfilePath != CPStarterDialog.newFile && cpfilePath != ""){
                beginAsyncGather(cpfilePath);                
            }
            else{
                //Empezamos de cero
                beginAsyncGather();
            }

            //Cargamos la base de datos ahora
            if (databasePath == CPStarterDialog.newFile) createNewDataBaseFile();
            else loadDBFile(databasePath);


        }


        internal void saveSession(){
            //Clausuramos todos los threads
            cursorThread.stopLooping();
            cursorThread.join();

            //Quito todos los resaltados
            highlightText(new Point(Application.ActiveDocument.Content.Start, Application.ActiveDocument.Content.End),
                CPSegstate.CLEAR
                );

            //Aviso en el task pane
            ((CPHelperControl)helperPanel.Control).Invoke(new System.Action(() =>{
                ((CPHelperControl)helperPanel.Control).feedLabel.Text = noSessionFound;

                //Limpio las listas
                ((CPHelperControl)helperPanel.Control).originList.Rows.Clear();

                ((CPHelperControl)helperPanel.Control).proposalsList.Rows.Clear();

            }));
            
            //Poner el ribbon de capuchin en estado idle
            Globals.Ribbons.CPRibbon.setButtonsAsIdle();

            //Guardo el doc en XML
            string cpfilePath = logIn.cpfilePath;

            if (cpfilePath != CPStarterDialog.newFile || cpfilePath != ""){
                docManager.saveCapuchinFile(((CPTextManager)textManager).inputText.segmentedText, cpfilePath);
            }
            else {
                docManager.saveCapuchinFile(((CPTextManager)textManager).inputText.segmentedText, null);
            }

            //Cierro la base de datos
            dbHandler.closeDB();

            //hago logout
            logIn.logOut();
        }

        #endregion

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
            
        }
        
#endregion
    }
}
