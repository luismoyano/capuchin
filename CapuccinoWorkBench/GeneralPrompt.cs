﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapuccinoWorkBench
{
    public partial class GeneralPrompt : Form {

        public String resultName;

        public GeneralPrompt()
        {
            InitializeComponent();
            resultName = "";//Magic, incrementar
        }

        private void acceptBtn_Click(object sender, EventArgs e){//Incrementar para asegurar buen nombre
            if (inputTxt.Text != "")
            {
                resultName = inputTxt.Text;
                this.DialogResult = DialogResult.OK;
                Close();
            }
        }

        
    }
}
