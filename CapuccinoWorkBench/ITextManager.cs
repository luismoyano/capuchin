﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Declaración de funcionalidades para el manejo del texto de entrada
/// </summary>
namespace CapuccinoWorkBench
{
    interface ITextManager
    {
        void getDocumentText();
        string getUnitByCursor(int cursor);

        string getProposalByUnit(int cursor);

        String[] getUnitsByRange(Point rng);

        Point getUnitRangeByCursor(int cursor);

        bool createUnitBySelection(Point rng, string txt);

        bool deleteUnitBySelection(Point rng);

        bool deleteUnitByCursor(int cursor);

        bool joinUnitsByRange(Point rng);

        bool splitUnit(int cursor);

        Point[] getUnitsSpanByRange(Point range);

        Point getFormerBlockSpan(int cursor);

        Point getLatterBlockSpan(int cursor);

        void saveProposalInCurrentUnit(string newTxt);

        void stackCurrentUnit(int cursor);

        string getCurrentOpposite(int cursor);

        void swapUnitNProp(int cursor);

        CPSegstate getUnitState(int cursor);

        void notifyCommit(int cursor);
    }
}
