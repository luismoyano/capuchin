﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapuccinoWorkBench
{

    public partial class CPShortcutsView : Form
    {

        private const string waiting = "Please arrange your shortcuts";
        private const string redundanciesFound = "Repeated shortcuts are found, please fix it before closing";

        public string[] blends;
        public string[] keys;

        public CPShortcutsView()
        {
            InitializeComponent();
            this.Shown += CPShortcutsView_Shown;   
        }

        private void CPShortcutsView_Shown(object sender, EventArgs e){
            feedLabel.Text = waiting;

            blends = new string[list.Items.Count];
            keys = new string[list.Items.Count];
        }

        private void OKbtn_Click(object sender, EventArgs e)
        {
            //Verificamos que no haya repetidos en la lista
            if (!areThereDuplicates()){

                postPopulate();//Poblamos los arrays usando el easylistview

                DialogResult = DialogResult.OK;
                Close();
            }
            else {
                feedLabel.Text = redundanciesFound;
            }
        }

        private void postPopulate(){
            for (int i = 0; i < list.Items.Count; i++){
                blends[i] = list.Items[i].SubItems[1].Text;
                keys[i] = list.Items[i].SubItems[2].Text;
            }
        }

        private bool areThereDuplicates(){
            
            for (int i = 0; i < list.Items.Count; i++) blends[i] = list.Items[i].SubItems[1].Text + list.Items[i].SubItems[2].Text;

            
            return (blends.Length > blends.Distinct().Count());//Linq shit
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
