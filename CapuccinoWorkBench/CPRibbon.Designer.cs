﻿namespace CapuccinoWorkBench
{
    partial class CPRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public CPRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CPRibbon));
            this.tab1 = this.Factory.CreateRibbonTab();
            this.CapuccinoWorkbench = this.Factory.CreateRibbonTab();
            this.cpGroup = this.Factory.CreateRibbonGroup();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.group2 = this.Factory.CreateRibbonGroup();
            this.buttons = this.Factory.CreateRibbonGroup();
            this.group3 = this.Factory.CreateRibbonGroup();
            this.beginBtn = this.Factory.CreateRibbonButton();
            this.saveBtn = this.Factory.CreateRibbonButton();
            this.EditModebtn = this.Factory.CreateRibbonButton();
            this.saveEditBtn = this.Factory.CreateRibbonButton();
            this.saveOriBtn = this.Factory.CreateRibbonButton();
            this.createUnitBtn = this.Factory.CreateRibbonButton();
            this.ignoreUnitBtn = this.Factory.CreateRibbonButton();
            this.joinUnitsBtn = this.Factory.CreateRibbonButton();
            this.splitUnitsBtn = this.Factory.CreateRibbonButton();
            this.focusPropsBtn = this.Factory.CreateRibbonButton();
            this.addTxtBtn = this.Factory.CreateRibbonButton();
            this.deleteTxtBtn = this.Factory.CreateRibbonButton();
            this.saveUnitToDbBtn = this.Factory.CreateRibbonButton();
            this.helperDisplayBtn = this.Factory.CreateRibbonButton();
            this.shortcutsBtn = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.CapuccinoWorkbench.SuspendLayout();
            this.cpGroup.SuspendLayout();
            this.group1.SuspendLayout();
            this.group2.SuspendLayout();
            this.buttons.SuspendLayout();
            this.group3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.ControlId.OfficeId = "CapuchinRibbon";
            this.tab1.Label = "CapuchinRibbon";
            this.tab1.Name = "tab1";
            // 
            // CapuccinoWorkbench
            // 
            this.CapuccinoWorkbench.Groups.Add(this.cpGroup);
            this.CapuccinoWorkbench.Groups.Add(this.group1);
            this.CapuccinoWorkbench.Groups.Add(this.group2);
            this.CapuccinoWorkbench.Groups.Add(this.buttons);
            this.CapuccinoWorkbench.Groups.Add(this.group3);
            this.CapuccinoWorkbench.Label = "Capuchin";
            this.CapuccinoWorkbench.Name = "CapuccinoWorkbench";
            // 
            // cpGroup
            // 
            this.cpGroup.Items.Add(this.beginBtn);
            this.cpGroup.Items.Add(this.saveBtn);
            this.cpGroup.Label = "Capuchin";
            this.cpGroup.Name = "cpGroup";
            // 
            // group1
            // 
            this.group1.Items.Add(this.EditModebtn);
            this.group1.Items.Add(this.saveEditBtn);
            this.group1.Items.Add(this.saveOriBtn);
            this.group1.Items.Add(this.createUnitBtn);
            this.group1.Items.Add(this.ignoreUnitBtn);
            this.group1.Items.Add(this.joinUnitsBtn);
            this.group1.Items.Add(this.splitUnitsBtn);
            this.group1.Items.Add(this.focusPropsBtn);
            this.group1.Label = "Units";
            this.group1.Name = "group1";
            // 
            // group2
            // 
            this.group2.Items.Add(this.addTxtBtn);
            this.group2.Items.Add(this.deleteTxtBtn);
            this.group2.Label = "Text Operations";
            this.group2.Name = "group2";
            // 
            // buttons
            // 
            this.buttons.Items.Add(this.saveUnitToDbBtn);
            this.buttons.Label = "Database";
            this.buttons.Name = "buttons";
            // 
            // group3
            // 
            this.group3.Items.Add(this.helperDisplayBtn);
            this.group3.Items.Add(this.shortcutsBtn);
            this.group3.Label = "Views";
            this.group3.Name = "group3";
            // 
            // beginBtn
            // 
            this.beginBtn.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.beginBtn.Image = ((System.Drawing.Image)(resources.GetObject("beginBtn.Image")));
            this.beginBtn.Label = "Begin session";
            this.beginBtn.Name = "beginBtn";
            this.beginBtn.ShowImage = true;
            this.beginBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.beginBtn_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.saveBtn.Image = ((System.Drawing.Image)(resources.GetObject("saveBtn.Image")));
            this.saveBtn.Label = "Save session";
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.ShowImage = true;
            this.saveBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.saveBtn_Click);
            // 
            // EditModebtn
            // 
            this.EditModebtn.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.EditModebtn.Image = ((System.Drawing.Image)(resources.GetObject("EditModebtn.Image")));
            this.EditModebtn.Label = "Edit mode";
            this.EditModebtn.Name = "EditModebtn";
            this.EditModebtn.ShowImage = true;
            this.EditModebtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.EditModebtn_Click);
            // 
            // saveEditBtn
            // 
            this.saveEditBtn.Image = ((System.Drawing.Image)(resources.GetObject("saveEditBtn.Image")));
            this.saveEditBtn.Label = "Save as Proposal";
            this.saveEditBtn.Name = "saveEditBtn";
            this.saveEditBtn.ShowImage = true;
            this.saveEditBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.saveEditBtn_Click);
            // 
            // saveOriBtn
            // 
            this.saveOriBtn.Image = ((System.Drawing.Image)(resources.GetObject("saveOriBtn.Image")));
            this.saveOriBtn.Label = "Save as Original";
            this.saveOriBtn.Name = "saveOriBtn";
            this.saveOriBtn.ShowImage = true;
            // 
            // createUnitBtn
            // 
            this.createUnitBtn.Image = ((System.Drawing.Image)(resources.GetObject("createUnitBtn.Image")));
            this.createUnitBtn.Label = "Create Unit";
            this.createUnitBtn.Name = "createUnitBtn";
            this.createUnitBtn.ShowImage = true;
            this.createUnitBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.createUnitBtn_Click);
            // 
            // ignoreUnitBtn
            // 
            this.ignoreUnitBtn.Image = ((System.Drawing.Image)(resources.GetObject("ignoreUnitBtn.Image")));
            this.ignoreUnitBtn.Label = "Ignore Unit";
            this.ignoreUnitBtn.Name = "ignoreUnitBtn";
            this.ignoreUnitBtn.ShowImage = true;
            this.ignoreUnitBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ignoreUnitBtn_Click);
            // 
            // joinUnitsBtn
            // 
            this.joinUnitsBtn.Image = ((System.Drawing.Image)(resources.GetObject("joinUnitsBtn.Image")));
            this.joinUnitsBtn.Label = "Join Units";
            this.joinUnitsBtn.Name = "joinUnitsBtn";
            this.joinUnitsBtn.ShowImage = true;
            this.joinUnitsBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.joinUnitsBtn_Click);
            // 
            // splitUnitsBtn
            // 
            this.splitUnitsBtn.Image = ((System.Drawing.Image)(resources.GetObject("splitUnitsBtn.Image")));
            this.splitUnitsBtn.Label = "Split Units";
            this.splitUnitsBtn.Name = "splitUnitsBtn";
            this.splitUnitsBtn.ShowImage = true;
            this.splitUnitsBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.splitUnitsBtn_Click);
            // 
            // focusPropsBtn
            // 
            this.focusPropsBtn.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.focusPropsBtn.Image = ((System.Drawing.Image)(resources.GetObject("focusPropsBtn.Image")));
            this.focusPropsBtn.Label = "Select from list";
            this.focusPropsBtn.Name = "focusPropsBtn";
            this.focusPropsBtn.ShowImage = true;
            this.focusPropsBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.focusPropsBtn_Click);
            // 
            // addTxtBtn
            // 
            this.addTxtBtn.Image = ((System.Drawing.Image)(resources.GetObject("addTxtBtn.Image")));
            this.addTxtBtn.Label = "Add text";
            this.addTxtBtn.Name = "addTxtBtn";
            this.addTxtBtn.ShowImage = true;
            // 
            // deleteTxtBtn
            // 
            this.deleteTxtBtn.Image = ((System.Drawing.Image)(resources.GetObject("deleteTxtBtn.Image")));
            this.deleteTxtBtn.Label = "Delete text";
            this.deleteTxtBtn.Name = "deleteTxtBtn";
            this.deleteTxtBtn.ShowImage = true;
            // 
            // saveUnitToDbBtn
            // 
            this.saveUnitToDbBtn.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.saveUnitToDbBtn.Image = ((System.Drawing.Image)(resources.GetObject("saveUnitToDbBtn.Image")));
            this.saveUnitToDbBtn.Label = "Save Proposal to DB";
            this.saveUnitToDbBtn.Name = "saveUnitToDbBtn";
            this.saveUnitToDbBtn.ShowImage = true;
            this.saveUnitToDbBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.saveUnitToDbBtn_Click);
            // 
            // helperDisplayBtn
            // 
            this.helperDisplayBtn.Image = ((System.Drawing.Image)(resources.GetObject("helperDisplayBtn.Image")));
            this.helperDisplayBtn.Label = "Show panel";
            this.helperDisplayBtn.Name = "helperDisplayBtn";
            this.helperDisplayBtn.ShowImage = true;
            this.helperDisplayBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.helperDisplayBtn_Click);
            // 
            // shortcutsBtn
            // 
            this.shortcutsBtn.Image = ((System.Drawing.Image)(resources.GetObject("shortcutsBtn.Image")));
            this.shortcutsBtn.Label = "Shortcuts";
            this.shortcutsBtn.Name = "shortcutsBtn";
            this.shortcutsBtn.ShowImage = true;
            this.shortcutsBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.shortcutsBtn_Click);
            // 
            // CPRibbon
            // 
            this.Name = "CPRibbon";
            this.RibbonType = "Microsoft.Word.Document";
            this.Tabs.Add(this.tab1);
            this.Tabs.Add(this.CapuccinoWorkbench);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.CPRibbon_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.CapuccinoWorkbench.ResumeLayout(false);
            this.CapuccinoWorkbench.PerformLayout();
            this.cpGroup.ResumeLayout(false);
            this.cpGroup.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.group2.ResumeLayout(false);
            this.group2.PerformLayout();
            this.buttons.ResumeLayout(false);
            this.buttons.PerformLayout();
            this.group3.ResumeLayout(false);
            this.group3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonTab CapuccinoWorkbench;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup buttons;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton createUnitBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton ignoreUnitBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton joinUnitsBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton splitUnitsBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group3;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton helperDisplayBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton saveUnitToDbBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton EditModebtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton shortcutsBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton saveEditBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton focusPropsBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup cpGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton beginBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton saveBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton saveOriBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group2;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton addTxtBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton deleteTxtBtn;
    }

    partial class ThisRibbonCollection
    {
        internal CPRibbon CPRibbon
        {
            get { return this.GetRibbon<CPRibbon>(); }
        }
    }
}
