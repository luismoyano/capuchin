Capuchin is a workbench system developed to help translators with their regular tasks
As a vsto Add-in, Capuchin is meant to work with word documents.

Capuchin features the following functionalities:

- Text segmentation into translation units.
- Translation suggestion based upon previous translations.
- Security encription


Capuchin is a piece of software developed by Luis Moyano

2016 - 2017

#############################################################################################################################

20161016:

Cambios:

- Añadido segundo Thread para leer la posición del cursor.
- Añadidos String Builders para mejorar la gestión de memoria en los segmentos

Known Issues

- El Text manager aún usa un String para el Raw data, esto es ineficiente y puede ser que todo el texto no quepa.

#############################################################################################################################

20161017:

Cambios:

- Añadido Timer para llamar la función del cursor en intervalos de tiempo razonables aumentando el rendimiento.
- Añadido StringBuilders como Raw data para evitar pérdidas de texto.
- añadido evento de cambio de posición del cursor.
- Añadida primera versión de algoritmo de split

Known issues:

- No sé qué carajo hace el getDocumentText del Text manager, igual lo quito...
- Nunca detengo el timer del Thread, hay que hacerlo...

#############################################################################################################################

20161019:

Cambios:

- Añadida versión mejorada del algoritmo de split
- Añadido método para detener el loopinf del timer.

Known issues:

- No sé qué carajo hace el getDocumentText del Text manager, igual lo quito...
- Los puntos y seguido dejan el espacio después del punto en el segmento, estaría bien quitarlo.

#############################################################################################################################

20161020:

Cambios:

- Versión Pre-Alfa1 terminada (a falta de testing con mejores casos)
- Ahora se guardan en los Segments los bounds de cada segmento.
- Añadido método para identificar en qué segmento está el cursor.
- Añadida llamada Thread safe para mostrar en el panel de orígenes el segmento en cuestion.
- Añadidos parámetros para hacer responsivo el layout del panel lateral.

Known issues:

- No sé qué carajo hace el getDocumentText del Text manager, igual lo quito...
- Los puntos y seguido dejan el espacio después del punto en el segmento, estaría bien quitarlo.

#############################################################################################################################

20161021:

Cambios:

- SQLite linkado
- Primera implementación de la interfaz de base de datos
Known issues:

- No sé qué carajo hace el getDocumentText del Text manager, igual lo quito...
- Los puntos y seguido dejan el espacio después del punto en el segmento, estaría bien quitarlo.
- El algoritmo no diferencia entre títulos y contenido
- El algoritmo no diferencia entre salto de línea.

#############################################################################################################################

20161024:

Cambios:

- Manager genérico de bases de datos implementado
- Añadidos formularios para crear bases de datos nuevas.
- Añadida capa intermedia que se pelee con la abstracción del manager.

Known issues:

- No sé qué carajo hace el getDocumentText del Text manager, igual lo quito...
- Los puntos y seguido dejan el espacio después del punto en el segmento, estaría bien quitarlo.
- El algoritmo no diferencia entre títulos y contenido
- El algoritmo no segmenta por salto de línea.
- Estoy bastante seguro de que las instrucciones de la DB se están haciendo mal, a lo largo de esta semana irán saliendo.